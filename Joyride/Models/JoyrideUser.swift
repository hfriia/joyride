//
//  JoyrideUser.swift
//  Joyride
//
//  Created by Friia, Harrison on 5/6/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import Foundation
import UIKit
import Parse

class JoyrideUser: PFUser, NSCoding {
    
    enum Keys {
        static let name = "name"
        static let birthday = "birthday"
        static let email = "email"
        static let password = "password"
        static let profileImageFile = "profileImageFile"
        static let points = "points"
        static let lastLoginDate = "lastLoginDate"
        static let bookmarks = "bookmarks"
    }

    @NSManaged var name: String
    @NSManaged var birthday: Date?
    @NSManaged var profileImageFile: PFFile?
    @NSManaged var points: Int
    @NSManaged var lastLoginDate: Date
    @NSManaged var bookmarks: [String]
    
    override init() {
        super.init()
    }

    init(name: String, birthday: Date?, email: String, password: String, profileImageFile: PFFile?, points: Int, lastLoginDate: Date, bookmarks: [String]) {
        super.init()
        self.name = name
        self.birthday = birthday
        self.email = email
        self.username = email
        self.password = password
        self.profileImageFile = profileImageFile
        self.points = points
        self.lastLoginDate = lastLoginDate
        self.bookmarks = bookmarks
    }

    required init?(coder aDecoder: NSCoder) {
        super.init()
        name = aDecoder.decodeObject(forKey: Keys.name) as! String
        birthday = aDecoder.decodeObject(forKey: Keys.birthday) as? Date
        email = aDecoder.decodeObject(forKey: Keys.email) as? String
        password = aDecoder.decodeObject(forKey: Keys.password) as? String

        if let image = aDecoder.decodeObject(forKey: Keys.profileImageFile) as? UIImage {
            if let data = UIImagePNGRepresentation(image), let file = PFFile(data: data) {
                self.profileImageFile = file
            }
        }
        
        points = aDecoder.decodeInteger(forKey: Keys.points)
        lastLoginDate = aDecoder.decodeObject(forKey: Keys.lastLoginDate) as! Date
        bookmarks = aDecoder.decodeObject(forKey: Keys.bookmarks) as! [String]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: Keys.name)
        aCoder.encode(birthday, forKey: Keys.birthday)
        aCoder.encode(email, forKey: Keys.email)
        aCoder.encode(password, forKey: Keys.password)

        do {
            if let dataImage = try profileImageFile?.getData(), let image = UIImage(data: dataImage) {
                aCoder.encode(image, forKey: Keys.profileImageFile)
            }            
        } catch {
            // Error
        }
        
        aCoder.encode(points, forKey: Keys.points)
        aCoder.encode(lastLoginDate, forKey: Keys.lastLoginDate)
        aCoder.encode(bookmarks, forKey: Keys.bookmarks)
    }
}
