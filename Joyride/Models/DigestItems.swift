//
//  ArticleDigestItem.swift
//  Joyride
//
//  Created by Harrison Friia on 7/8/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import Parse

class DigestItem: PFObject {
    enum Keys {
        static let authorImageFile = "authorImageFile"
        static let authorName = "authorName"
        static let titleText = "titleText"
        static let bodyText = "bodyText"
        static let position = "position"
        static let approved = "approved"
    }

    @NSManaged var authorImageFile: PFFile?
    @NSManaged var authorName: String
    @NSManaged var titleText: String
    @NSManaged var bodyText: String
    @NSManaged var position: Int
    @NSManaged var approved: Bool

    init(authorImageFile: PFFile?, authorName: String, titleText: String, bodyText: String, position: Int, approved: Bool) {
        super.init()
        self.authorImageFile = authorImageFile
        self.authorName = authorName
        self.titleText = titleText
        self.bodyText = bodyText
        self.position = position
        self.approved = approved
    }
    
    override init() {
        super.init()
    }

//    required init?(coder aDecoder: NSCoder) {
//        super.init()
//
//        if let image = aDecoder.decodeObject(forKey: Keys.authorImageFile) as? UIImage {
//            if let data = UIImagePNGRepresentation(image), let file = PFFile(data: data) {
//                self.authorImageFile = file
//            }
//        }
//
//        self.authorName = aDecoder.decodeObject(forKey: Keys.authorName) as! String
//        self.titleText = aDecoder.decodeObject(forKey: Keys.titleText) as! String
//        self.bodyText = aDecoder.decodeObject(forKey: Keys.bodyText) as! String
//        self.position = aDecoder.decodeObject(forKey: Keys.position) as? Int ?? 999
//        self.approved = aDecoder.decodeBool(forKey: Keys.approved)
//    }
//
//    func encode(with aCoder: NSCoder) {
//        do {
//            let dataImage = try authorImageFile.getData()
//            let image = UIImage(data: dataImage)!
//            aCoder.encode(image, forKey: Keys.authorImageFile)
//        } catch {
//            // Error
//        }
//
//        aCoder.encode(self.authorName, forKey: Keys.authorName)
//        aCoder.encode(self.titleText, forKey: Keys.titleText)
//        aCoder.encode(self.bodyText, forKey: Keys.bodyText)
//        aCoder.encode(self.position, forKey: Keys.position)
//        aCoder.encode(self.approved, forKey: Keys.approved)
//    }
}

class ArticleDigestItem: DigestItem {
    enum Keys {
        static let originText = "originText"
        static let originName = "originName"
        static let originLink = "originLink"
        static let extraImageFile = "extraImageFile"
    }

    @NSManaged var originText: String?
    @NSManaged var originName: String?
    @NSManaged var originLink: String?
    @NSManaged var extraImageFile: PFFile?
    
    override init() {
        super.init()
    }

    init(authorImageFile: PFFile?, authorName: String, titleText: String, bodyText: String, position: Int, approved: Bool, originText: String?, originName: String?, originLink: String?, extraImageFile: PFFile?) {
        super.init(authorImageFile: authorImageFile, authorName: authorName, titleText: titleText, bodyText: bodyText, position: position, approved: approved)
        
        self.originText = originText
        self.originName = originName
        self.originLink = originLink
        self.extraImageFile = extraImageFile
        self.position = position
        self.approved = approved
    }
}

extension ArticleDigestItem: PFSubclassing {

    class func parseClassName() -> String {
        return "articleDigestItem"
    }
}

class VideoDigestItem: DigestItem {
    enum Keys {
        static let videoURL = "videoURL"
    }

    @NSManaged var videoURL: String
    
    override init() {
        super.init()
    }

    init(authorImageFile: PFFile, authorName: String, titleText: String, bodyText: String, position: Int, approved: Bool, videoURL: String) {
        super.init(authorImageFile: authorImageFile, authorName: authorName, titleText: titleText, bodyText: bodyText, position: position, approved: approved)
        self.videoURL = videoURL
    }
}

extension VideoDigestItem: PFSubclassing {
    
    class func parseClassName() -> String {
        return "videoDigestItem"
    }
}
