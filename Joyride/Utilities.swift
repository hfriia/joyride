//
//  Utilities.swift
//  Joyride
//
//  Created by Harrison Friia on 6/15/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import UserNotifications

class BookmarksDataStore {
    static let sharedInstance = BookmarksDataStore()
    private init() {}
    var bookmarks = [DigestItem]()
}

struct JoyrideConstants {
    static let digestPostFontSize: CGFloat = 15.0
    
    static let birthdayDateFormat = "MM-dd-yyyy"
    static let iso8061Format = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
    
    static let notificationOptions: UNAuthorizationOptions = [.badge]

    static var bookmarksFilePath: String {
        let manager = FileManager.default

        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first

        return (url!.appendingPathComponent("Bookmarks").path)
    }
    
    static var usersFilePath: String {
        let manager = FileManager.default
        
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        
        return (url!.appendingPathComponent("Users").path)
    }
}

struct JoyrideFonts {

    static let thinFontName = "SanFranciscoDisplay-Thin"
    static let regularFontName = "SanFranciscoDisplay-Regular"
    static let mediumFontName = "SanFranciscoDisplay-Medium"
    static let boldFontName = "SanFranciscoDisplay-Bold"

    static func thinFont(ofSize size: CGFloat) -> UIFont {
        var font = UIFont.systemFont(ofSize: size)
        if let customFont = UIFont(name: thinFontName, size: size) {
            font = customFont
        }

        return font
    }

    static func regularFont(ofSize size: CGFloat) -> UIFont {
        var font = UIFont.systemFont(ofSize: size)
        if let customFont = UIFont(name: regularFontName, size: size) {
            font = customFont
        }

        return font
    }

    static func mediumFont(ofSize size: CGFloat) -> UIFont {
        var font = UIFont.systemFont(ofSize: size)
        if let customFont = UIFont(name: mediumFontName, size: size) {
            font = customFont
        }

        return font
    }

    static func boldFont(ofSize size: CGFloat) -> UIFont {
        var font = UIFont.systemFont(ofSize: size)
        if let customFont = UIFont(name: boldFontName, size: size) {
            font = customFont
        }

        return font
    }
}

struct JoyrideControls {
    static func backButton(target: Any?, selector: Selector) -> UIBarButtonItem {
        let leftButton = UIButton(type: .custom)

        leftButton.frame = CGRect(x: 0, y: 0, width: 11, height: 18)
        leftButton.setBackgroundImage(#imageLiteral(resourceName: "BackArrow"), for: .normal)
        leftButton.backgroundColor = .clear

        leftButton.addTarget(target, action: selector, for: .touchUpInside)

        let leftView = UIView(frame: CGRect(x: 0, y: -5, width: 80, height: 30))
        leftView.backgroundColor = .clear
        leftView.bounds = leftView.frame.insetBy(dx: 5, dy: 0)

        let gestureRecognizer = UITapGestureRecognizer.init(target: target, action: selector)
        leftView.addGestureRecognizer(gestureRecognizer)

        leftView.addSubview(leftButton)

        return UIBarButtonItem(customView: leftView)
    }

    static func plusButton(target: Any?, selector: Selector?) -> UIBarButtonItem {
        let leftButton = UIButton(type: .custom)

        leftButton.frame = CGRect(x: 0, y: 0, width: 35, height: 36)
        leftButton.backgroundColor = .clear
        leftButton.setImage(#imageLiteral(resourceName: "PlusIcon"), for: .normal)
        leftButton.setImage(#imageLiteral(resourceName: "PlusIcon"), for: .highlighted)
        leftButton.contentHorizontalAlignment = .fill
        leftButton.contentVerticalAlignment = .fill
        leftButton.imageView?.contentMode = .scaleAspectFit

        if let selector = selector {
            leftButton.addTarget(target, action: selector, for: .touchUpInside)
        }

        let leftView = UIView(frame: CGRect(x: -10, y: 4, width: 35, height: 36))
        leftView.backgroundColor = .clear
        leftView.bounds = leftView.frame.insetBy(dx: 0, dy: 0)

        let gestureRecognizer = UITapGestureRecognizer.init(target: target, action: selector)
        leftView.addGestureRecognizer(gestureRecognizer)

        leftView.addSubview(leftButton)

        return UIBarButtonItem(customView: leftView)
    }
}

struct JoyrideUtilities {
    static func birthdayDatePicker(selector: Selector?) -> UIDatePicker {
        let datePicker = UIDatePicker()
        if let selector = selector {
            datePicker.addTarget(self, action: selector, for: .valueChanged)
        }
        
        let now = Date()
        var minusHundredYears = DateComponents()
        minusHundredYears.year = -100
        let hundredYearsAgo = Calendar.current.date(byAdding: minusHundredYears, to: now)
        
        var minusThirteenYears = DateComponents()
        minusThirteenYears.year = -13
        let thirteenYearsAgo = Calendar.current.date(byAdding: minusThirteenYears, to: now)
        
        datePicker.maximumDate = thirteenYearsAgo
        datePicker.minimumDate = hundredYearsAgo
        
        datePicker.datePickerMode = .date
        
        return datePicker
    }
}

extension Date {
    var birthdayDateString: String {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = JoyrideConstants.birthdayDateFormat
            return formatter.string(from: self)
        }
    }
    
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = JoyrideConstants.iso8061Format
        return formatter
    }()
    
    static let birthdayFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = JoyrideConstants.birthdayDateFormat
        return formatter
    }()
}

extension String {
    var dateFromISO8601: Date? {
        return Formatter.iso8601.date(from: self)   // "Mar 22, 2017, 10:22 AM"
    }
}

class EmailTextFieldValidator {
    func validate(field: UITextField) -> String? {
        guard let trimmedText = field.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return nil
        }
        
        guard let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else {
            return nil
        }
        
        let range = NSMakeRange(0, NSString(string: trimmedText).length)
        let allMatches = dataDetector.matches(in: trimmedText,
                                              options: [],
                                              range: range)
        
        if allMatches.count == 1,
            allMatches.first?.url?.absoluteString.contains("mailto:") == true
        {
            return trimmedText
        }
        return nil
    }
}
