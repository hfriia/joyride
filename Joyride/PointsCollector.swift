//
//  PointsCollector.swift
//  Joyride
//
//  Created by Harrison Friia on 8/15/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import Foundation
import Parse

struct PointsCollector {
    
    enum Keys {
        static let usersKey = "users"
        static let pointsKey = "points"
        static let emailKey = "email"
        static let lastLogin = "lastLogin"
    }
    
    enum PointsEvent: Int {
        case dailyCheckIn = 3
        case shareTipsAndTricks = 8
        case activityAchievements = 12
        case referAFriend = 30
        case empowerWomen = 85
        case writeAReview = 50
        case productIdeas =  10
        case sponsoredContent = 20
        case birthday = 75
    }
    
    static var currentUser: JoyrideUser? {
        return JoyrideUser.current()
    }
    
    static func addPoints(_ points: PointsEvent) {
        
//        guard let currentUser = currentUser else {
//            return
//        }
//
//        if points == .dailyCheckIn && hasLoggedInToday(user: currentUser) {
//            // If we're trying to give them daily check in points but they've
//            // already logged in today, do nothing.
//            return
//        }
//
//        if points == .birthday && hasLoggedInToday(user: currentUser) {
//            return
//        }
//
//        if points == .dailyCheckIn {
//            currentUser.lastLoginDate = Date()
//        }
//
//        currentUser.points += points.rawValue
//
//        currentUser.saveInBackground()
    }

    static func hasLoggedInToday(user: JoyrideUser) -> Bool {
        // Difference in calendar days from now to the last login date
        let result = Date().interval(ofComponent: .day, fromDate: user.lastLoginDate)
        
        // See if the difference measured in days is greater than 0.
        if result > 0 {
            // It's been at least 1 day since they've logged in
            return false
        }
        
        // They've logged in once in the past day.
        return true
    }
}

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}
