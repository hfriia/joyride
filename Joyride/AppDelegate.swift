//
//  AppDelegate.swift
//  Joyride
//
//  Created by Harrison Friia on 2/16/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import UserNotifications
import Parse
import iRate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var currentUser: JoyrideUser? {
        return JoyrideUser.current()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        iRate.sharedInstance().verboseLogging = false

        let configuration = ParseClientConfiguration {
            $0.applicationId = "com.joyride.joyride"
            $0.server = "https://joyrideserver.herokuapp.com/parse"
        }
        Parse.initialize(with: configuration)
        
        JoyrideUser.current()?.fetchInBackground(block: { (user, error) in
            if let error = error {
                print(error.localizedDescription)
                
                if (error as NSError).code == 209 {
                    JoyrideUser.logOut()
                }
            }
        })
        
        if currentUser == nil {
            PFUser.enableAutomaticUser()
            PFUser.current()?.incrementKey("RunCount")
            PFUser.current()?.saveInBackground()
        }

        registerParseSubclasses()
        subscribeToNotifications()
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        guard let user = JoyrideUser.current() else { return }
        
        if let birthday = user.birthday {
            let userCalendar = NSCalendar.current
            let requestedComponent: Set<Calendar.Component> = [.year, .month, .day]
            let timeDifference = userCalendar.dateComponents(requestedComponent, from: birthday, to: Date())
            if let dayDifference = timeDifference.day,
                let monthDifference = timeDifference.month,
                dayDifference == 0,
                monthDifference == 0 {
                
                PointsCollector.addPoints(.birthday)
            }
        }
        
        PointsCollector.addPoints(.dailyCheckIn)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let installation = PFInstallation.current()
        installation?.setDeviceTokenFrom(deviceToken)
        installation?.saveInBackground()
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        if (error as NSError).code == 3010 {
            print("Push notifications are not supported in the iOS Simulator.")
        } else {
            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error)
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if let currentUser = currentUser {
            NSKeyedArchiver.archiveRootObject(currentUser, toFile: JoyrideConstants.usersFilePath)
        }
    }
    
    func subscribeToNotifications() {
        let userNotificationCenter = UNUserNotificationCenter.current()
        userNotificationCenter.delegate = self
        
        userNotificationCenter.requestAuthorization(options: JoyrideConstants.notificationOptions) { accepted, error in
            guard accepted == true else {
                print("User declined remote notifications")
                return
            }
            
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            
            // Create and register a local notification to display daily at 5am EST
            let amContent = UNMutableNotificationContent()
            amContent.badge = 1
            
            var amDateComponents = DateComponents()
            amDateComponents.calendar = Calendar.current
            amDateComponents.timeZone = TimeZone.init(identifier: "America/New_York")
            amDateComponents.hour = 5
            amDateComponents.minute = 0
            
            let amTrigger = UNCalendarNotificationTrigger(dateMatching: amDateComponents, repeats: true)
            
            let amIdentifier = "DailyDigest5AMNotification"
            let amRequest = UNNotificationRequest(identifier: amIdentifier,
                                                content: amContent,
                                                trigger: amTrigger)
            
            userNotificationCenter.add(amRequest) { error in
                if let error = error {
                    print("Error adding local notification request: \(error)")
                }
            }
            
            // Create and register a local notification to display daily at 5pm EST
//            let pmContent = UNMutableNotificationContent()
//            pmContent.badge = 1
//            
//            var pmDateComponents = DateComponents()
//            pmDateComponents.calendar = Calendar.current
//            pmDateComponents.timeZone = TimeZone.init(identifier: "America/New_York")
//            pmDateComponents.hour = 17
//            pmDateComponents.minute = 0
//            
//            let pmTrigger = UNCalendarNotificationTrigger(dateMatching: pmDateComponents, repeats: true)
//            
//            let pmIdentifier = "DailyDigest5PMNotification"
//            let pmRequest = UNNotificationRequest(identifier: pmIdentifier,
//                                                  content: pmContent,
//                                                  trigger: pmTrigger)
//            
//            userNotificationCenter.add(pmRequest) { error in
//                if let error = error {
//                    print("Error adding local notification request: \(error)")
//                }
//            }
        }
    }

    func registerParseSubclasses() {
        ArticleDigestItem.registerSubclass()
        VideoDigestItem.registerSubclass()
        JoyrideUser.registerSubclass()
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler:
        @escaping (UNNotificationPresentationOptions) -> Void) {
        PFPush.handle(notification.request.content.userInfo)
        completionHandler(.alert)
    }
}
