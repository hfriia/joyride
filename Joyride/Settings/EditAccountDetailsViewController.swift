//
//  EditAccountDetailsViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 10/22/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

enum EditMode {
    case name
    case birthday
    case email
    case password
}

class EditAccountDetailsViewController: UIViewController {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var navItem: UINavigationItem!
    
    var mode: EditMode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = false
        
        let backButton = JoyrideControls.backButton(target: self, selector: #selector(returnToAccount))
        navItem.leftBarButtonItem = backButton
    }
    
    func configure() {
        guard let mode = mode else { return }
        
        switch mode {
        case .name:
            navItem.title = "Edit Name"
        case .birthday:
            navItem.title = "Edit Birthday"
            textField.inputView = JoyrideUtilities.birthdayDatePicker(selector: #selector(didChangeDate(sender:)))
        case .email:
            navItem.title = "Edit Email"
        case .password:
            navItem.title = "Edit Password"
            textField.isSecureTextEntry = true
        }
        
        saveButton.layer.cornerRadius = 6
        saveButton.layer.borderColor = UIColor.red.cgColor
        saveButton.layer.borderWidth = 1.5
        saveButton.backgroundColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configure()
    }

    @IBAction func save(_ sender: UIButton) {
        guard let mode = mode,
            let user = JoyrideUser.current(),
            let text = textField.text else {
                return
        }

        switch mode {
        case .name:
            user.name = text
            
        case .birthday:
            if let birthdayDate = DateFormatter.birthdayFormatter.date(from: text) {
                user.birthday = birthdayDate
            }
            
        case .email:
            if let email = EmailTextFieldValidator().validate(field: textField) {
                user.email = email
            }
            
        case .password:
            user.password = text // Not sure if you can change password like this
        }
        
        user.saveInBackground { [weak self] (success, error) in
            if success {
                self?.returnToAccount()
            }
        }
    }
    
    func didChangeDate(sender: UIDatePicker) {
        textField.text = sender.date.birthdayDateString
    }
    
    func returnToAccount() {
        textField.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
}
