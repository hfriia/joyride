//
//  AccountSettingsTableViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 6/9/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import Parse

class AccountSettingsTableViewController: UITableViewController {

    var user: JoyrideUser? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.currentUser
    }
    
    lazy var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        // Hide empty cells at the bottom
        tableView.tableFooterView = UIView(frame: .zero)
        
        imagePicker.delegate = self

        let backButton = JoyrideControls.backButton(target: self, selector: #selector(backButtonPressed(sender:)))
        navigationItem.leftBarButtonItem = backButton
    }
    
    func startEditing() {
        
    }

    func backButtonPressed(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    // MARK: - Table view delegate methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 57
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = UITableViewCell()
        
        guard let user = user else {
            return cell
        }
        
        cell.textLabel?.textColor = UIColor.darkText

        switch indexPath.row {
        case 0:
            cell.textLabel?.text = user.name
        case 1:
            if let bday = user.birthday?.birthdayDateString {
                cell.textLabel?.text = bday
            } else {
                cell.textLabel?.textColor = UIColor.gray
                cell.textLabel?.text = "Birthday"
            }
        case 2:
            cell.textLabel?.text = user.email
        case 3:
            cell.textLabel?.text = "********"
        case 4:
            if user.profileImageFile != nil {
                cell.textLabel?.text = "Profile Photo"
            } else {
                cell.textLabel?.textColor = UIColor.gray
                cell.textLabel?.text = "Profile Photo (Optional)"
            }
        default:
            return cell
        }
        
        cell.textLabel?.font = JoyrideFonts.regularFont(ofSize: 15)
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = .zero
        cell.layoutMargins = .zero

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 4 {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            
            guard let types = UIImagePickerController.availableMediaTypes(for: .photoLibrary) else {
                return
            }
            
            imagePicker.mediaTypes = types
            
            present(imagePicker, animated: true, completion: nil)
        }
        
        performSegue(withIdentifier: "editAccount", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var mode: EditMode?
        
        guard let indexPath = tableView.indexPathForSelectedRow else {
            return
        }
        
        switch indexPath.row {
        case 0:
            mode = .name
        case 1:
            mode = .birthday
        case 2:
            mode = .email
        case 3:
            mode = .password
        default:
            mode = .name
        }
        
        let vc = segue.destination as? EditAccountDetailsViewController
        vc?.mode = mode
    }
    
    func setProfilePhoto(image: UIImage) {
        if let data = UIImagePNGRepresentation(image),
            let file = PFFile(data: data) {
            user?.profileImageFile = file
            user?.saveInBackground()
        }
    }
}

extension AccountSettingsTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            setProfilePhoto(image: image)
            imagePicker.dismiss(animated: true, completion: nil)
            
        } else {
            print("Something went wrong")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
