//
//  BookmarksTableViewModel.swift
//  Joyride
//
//  Created by Harrison Friia on 7/30/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import Foundation
import Parse

protocol BookmarksTableViewModelDelegate: class {
    func digestItemsAvailable(_ items: [DigestItem])
    func networkErrorReturned(_ error: Error)
}

class BookmarksTableViewModel {

    var numberOfRowsInSection: Int {
        return digestItems.count
    }

    let numberOfSections = 1

    weak var delegate: BookmarksTableViewModelDelegate?
    
    var digestItems = [DigestItem]() {
        didSet {
            delegate?.digestItemsAvailable(digestItems)
        }
    }
    
    init() {
        retrieveDigestItems()
    }

    func viewModelForCell(at index: Int) -> Any? {
        guard index < digestItems.count else { return nil }

        if let articleItem = digestItems[index] as? ArticleDigestItem {
            return ArticleTableCellViewModel(digestItem: articleItem)
        }

        if let videoItem = digestItems[index] as? VideoDigestItem {
            return VideoTableCellViewModel(digestItem: videoItem)
        }

        return nil
    }

    func deleteBookmarkItem(at index: Int) {
        guard index < digestItems.count else { return }

        digestItems.remove(at: index)
        
        let user = JoyrideUser.current()
        user?.bookmarks.remove(at: index)
        user?.saveInBackground()
    }
}

// MARK: - Networking
extension BookmarksTableViewModel {
    func retrieveDigestItems() {
        
        var tempItems = [DigestItem]()
        var lastError: Error?
        
        let queue = DispatchQueue(label: "com.joyride.joyride.digestItemRequest", attributes: .concurrent)
        let group = DispatchGroup()
        
        group.enter()
        queue.async(group: group) {
            self.getVideoDigestItems(completion: { (items, error) in
                if let items = items, error == nil {
                    tempItems += items
                }
                lastError = error
                group.leave()
            })
        }
        
        group.enter()
        queue.async(group: group) {
            self.getArticleDigestItems(completion: { (items, error) in
                if let items = items, error == nil {
                    tempItems += items
                }
                lastError = error
                group.leave()
            })
        }
        
        group.notify(queue: queue) { [weak self] in
            group.wait()
            DispatchQueue.main.async {
                
                if let lastError = lastError, tempItems.count == 0 {
                    self?.delegate?.networkErrorReturned(lastError)
                }
                
                guard let bookmarks = JoyrideUser.current()?.bookmarks else {
                    return
                }
                
                // Zip and sort didn't work, so sort them manually here
                var result = [DigestItem]()
                for id in bookmarks {
                    for item in tempItems {
                        if item.objectId == id {
                            result.append(item)
                        }
                    }
                }
                
                self?.digestItems = result
            }
        }
    }
    
    func getArticleDigestItems(completion: @escaping ([DigestItem]?, Error?) -> Void) {
        
        guard let bookmarks = JoyrideUser.current()?.bookmarks else {
            return
        }
        
        let query = PFQuery(className:"articleDigestItem")
        query.whereKey("objectId", containedIn: bookmarks)

        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                if let objects = objects {
                    let items = self.toArticleDigestItems(objects)
                    completion(items, nil)
                }
            } else {
                print("Error: \(error.debugDescription)")
                completion(nil, error)
            }
        }
    }
    
    func toArticleDigestItems(_ items: [PFObject]) -> [ArticleDigestItem] {
        
        var articleItems = [ArticleDigestItem]()
        
        for item in items {
            
            guard let authorName = item["authorName"] as? String,
                let titleText = item["titleText"] as? String,
                let bodyText = item["bodyText"] as? String,
                let position = item["position"] as? Int,
                let authorImageFile = item["authorImageFile"] as? PFFile else {
                    continue
            }
            
            let originText = item["originText"] as? String
            let originName = item["originName"] as? String
            let originLink = item["originLink"] as? String
            let approved = item["approved"] as? Bool ?? false
            
            let extraImageFile = item["extraImage"] as? PFFile
            
            let articleItem = ArticleDigestItem(authorImageFile: authorImageFile,
                                                authorName: authorName,
                                                titleText: titleText,
                                                bodyText: bodyText,
                                                position: position,
                                                approved: approved,
                                                originText: originText,
                                                originName: originName,
                                                originLink: originLink,
                                                extraImageFile: extraImageFile)
            articleItem.objectId = item.objectId
            
            articleItems.append(articleItem)
        }
        
        return articleItems
    }
    
    func getVideoDigestItems(completion: @escaping ([DigestItem]?, Error?) -> Void) {
        
        guard let bookmarks = JoyrideUser.current()?.bookmarks else {
            return
        }
        
        let query = PFQuery(className:"videoDigestItem")
        query.whereKey("objectId", containedIn: bookmarks)

        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                if let objects = objects {
                    // For some reason casting the array to [VideoDigestItem] doesn't work,
                    // so map it manually with this method
                    let items = self.toVideoDigestItems(objects)
                    completion(items, nil)
                }
            } else {
                print("Error: \(error.debugDescription)")
                completion(nil, error)
            }
        }
    }
    
    func toVideoDigestItems(_ items: [PFObject]) -> [VideoDigestItem] {
        
        var videoItems = [VideoDigestItem]()
        
        for item in items {
            
            guard let authorName = item["authorName"] as? String,
                let titleText = item["titleText"] as? String,
                let bodyText = item["bodyText"] as? String,
                let position = item["position"] as? Int,
                let videoURLString = item["videoURL"] as? String,
                let imageFile = item["authorImage"] as? PFFile else {
                    break
            }
            
            let approved = item["approved"] as? Bool ?? false
            
            let videoItem = VideoDigestItem(authorImageFile: imageFile,
                                            authorName: authorName,
                                            titleText: titleText,
                                            bodyText: bodyText,
                                            position: position,
                                            approved: approved,
                                            videoURL: videoURLString)
            
            videoItem.objectId = item.objectId
            
            videoItems.append(videoItem)
        }
        
        return videoItems
    }
}
