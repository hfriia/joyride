//
//  SettingsTableViewCell.swift
//  Joyride
//
//  Created by Friia, Harrison on 6/20/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    override func layoutSubviews() {
        super.layoutSubviews()

        var adjustedFrame = accessoryView?.frame
        adjustedFrame?.origin.x += 8.0
        accessoryView?.frame = adjustedFrame!
    }
}
