//
//  BookmarksViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 7/30/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class BookmarksViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let articleCellIdentifier = "ArticleTableViewCell"
    let videoCellIdentifier = "VideoTableViewCell"
    let footerCellIdentifier = "FooterCell"

    lazy var viewModel = BookmarksTableViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        automaticallyAdjustsScrollViewInsets = false
        
        viewModel.delegate = self

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .singleLine
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 199

        // This hides the extra cell separator at the end
        tableView.tableFooterView = UIView()

//        let backButton = JoyrideControls.backButton(target: self, selector: #selector(backButtonPressed(sender:)))
//        navigationItem.leftBarButtonItem = backButton
        
        let leftButton = UIButton(type: .custom)
        
        if let font = UIFont(name: "SanFranciscoDisplay-Thin", size: 20.5) {
            let title = NSAttributedString(string: "X", attributes: [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.darkText])
            leftButton.setAttributedTitle(title, for: .normal)
        }
        
        leftButton.backgroundColor = .clear
        leftButton.sizeToFit()
        leftButton.addTarget(self, action: #selector(returnToDigest), for: .touchUpInside)
        
        let leftView = UIView(frame: CGRect(x: 0, y: 4, width: 40, height: 30))
        leftView.backgroundColor = .clear
        leftView.bounds = leftView.frame.insetBy(dx: 15, dy: 0)
        
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(returnToDigest))
        leftView.addGestureRecognizer(gestureRecognizer)
        
        leftView.addSubview(leftButton)
        
        let leftBarButton = UIBarButtonItem(customView: leftView)
        
        navigationItem.leftBarButtonItem = leftBarButton
    }

    func backButtonPressed(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func returnToDigest() {
        let settingsTransition = CATransition()
        settingsTransition.duration = 0.3
        settingsTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        settingsTransition.type = kCATransitionPush
        settingsTransition.subtype = kCATransitionFromRight
        view.window?.layer.add(settingsTransition, forKey: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func settingsSwiped(_ sender: UISwipeGestureRecognizer) {
        
        returnToDigest()
        
//        let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc = sb.instantiateViewController(withIdentifier: "digestViewController")
//
//        let settingsTransition = CATransition()
//        settingsTransition.duration = 0.3
//        settingsTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        settingsTransition.type = kCATransitionPush
//        settingsTransition.subtype = kCATransitionFromRight
//        view.window?.layer.add(settingsTransition, forKey: nil)
//
//        navigationController?.show(vc, sender: self)
    }
}

extension BookmarksViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension BookmarksViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if viewModel.numberOfRowsInSection == 0 {
            tableView.separatorStyle = .none
        } else {
            tableView.separatorStyle = .singleLine
        }

        return viewModel.numberOfRowsInSection
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let leftDeleteButton = MGSwipeButton(title: "Delete",
                                           backgroundColor: .red,
                                           callback: nil)
        let rightDeleteButton = MGSwipeButton(title: "Delete",
                                            backgroundColor: .red,
                                            callback: nil)

        if let articleViewModel = viewModel.viewModelForCell(at: indexPath.row) as? ArticleTableCellViewModel {
            let cell = tableView.dequeueReusableCell(withIdentifier: articleCellIdentifier, for: indexPath) as! ArticleTableViewCell

            cell.viewModel = articleViewModel
            // Full width separator line
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero

            cell.delegate = self
            cell.leftButtons = [leftDeleteButton]
            cell.rightButtons = [rightDeleteButton]
            
            cell.leftExpansion.expansionColor = .red
            cell.leftExpansion.buttonIndex = 0
            
            cell.rightExpansion.expansionColor = .red
            cell.rightExpansion.buttonIndex = 0

            return cell
        }

        if let videoViewModel = viewModel.viewModelForCell(at: indexPath.row) as? VideoTableCellViewModel {
            let cell = tableView.dequeueReusableCell(withIdentifier: videoCellIdentifier, for: indexPath) as! VideoTableViewCell

            cell.viewModel = videoViewModel
            // Full width separator line
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero

            cell.delegate = self
            cell.leftButtons = [leftDeleteButton]
            cell.rightButtons = [rightDeleteButton]
            
            cell.leftExpansion.expansionColor = .red
            cell.leftExpansion.buttonIndex = 0
            
            cell.rightExpansion.expansionColor = .red
            cell.rightExpansion.buttonIndex = 0

            return cell
        }

        let cell = UITableViewCell()

        // Bit of a hack to hide the last cell separator
        cell.separatorInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, cell.bounds.size.width * 1.2)

        return cell
    }
}

extension BookmarksViewController: MGSwipeTableCellDelegate {
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {

        if let indexPath = tableView.indexPath(for: cell) {
            viewModel.deleteBookmarkItem(at: indexPath.row)
            tableView.reloadData()
        }

        return true
    }
}

extension BookmarksViewController: BookmarksTableViewModelDelegate {
    func digestItemsAvailable(_ items: [DigestItem]) {
        tableView.reloadData()
    }
    
    func networkErrorReturned(_ error: Error) {
        let alertVC = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { action in
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(action)
        present(alertVC, animated: true, completion: nil)
    }
}
