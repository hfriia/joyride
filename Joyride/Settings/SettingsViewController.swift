//
//  SettingsViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 6/9/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import Parse

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let cellNames = ["Account", "Push Notification", "Bookmarks", "Earn Points", "Redeem Points", "Chat with Us", "Sponsored Posts", "Privacy Policy", "Terms of Service"]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(SettingsTableViewCell.classForCoder(), forCellReuseIdentifier: "settingsCell")

        navigationController?.navigationBar.tintColor = UIColor.black
        automaticallyAdjustsScrollViewInsets = false

        tableView.delegate = self
        tableView.dataSource = self
        // Hide empty cells at the bottom
        tableView.tableFooterView = UIView(frame: .zero)

        let leftButton = UIButton(type: .custom)

        if let font = UIFont(name: "SanFranciscoDisplay-Thin", size: 20.5) {
            let title = NSAttributedString(string: "X", attributes: [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.darkText])
            leftButton.setAttributedTitle(title, for: .normal)
        }

        leftButton.backgroundColor = .clear
        leftButton.sizeToFit()
        leftButton.addTarget(self, action: #selector(returnToDigest), for: .touchUpInside)

        let leftView = UIView(frame: CGRect(x: 0, y: 4, width: 40, height: 30))
        leftView.backgroundColor = .clear
        leftView.bounds = leftView.frame.insetBy(dx: 15, dy: 0)

        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(returnToDigest))
        leftView.addGestureRecognizer(gestureRecognizer)

        leftView.addSubview(leftButton)

        let leftBarButton = UIBarButtonItem(customView: leftView)

        navigationItem.leftBarButtonItem = leftBarButton
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func showAlertForError(_ error: Error) {
        let errorVC = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        errorVC.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            errorVC.dismiss(animated: true, completion: nil)
        }))
        self.present(errorVC, animated: true, completion: nil)
    }

    func logOut() {
        JoyrideUser.logOut()
        // This will pop us to the splash screen controller, which will then redirect to the login screen
        self.navigationController?.popToRootViewController(animated: false)
    }

    func returnToDigest() {
        let settingsTransition = CATransition()
        settingsTransition.duration = 0.3
        settingsTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        settingsTransition.type = kCATransitionPush
        settingsTransition.subtype = kCATransitionFromRight
        view.window?.layer.add(settingsTransition, forKey: nil)
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func homeButtonTapped(_ sender: Any) {
        navigationController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func settingsSwiped(_ sender: UISwipeGestureRecognizer) {
        let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = sb.instantiateViewController(withIdentifier: "digestViewController")

        let settingsTransition = CATransition()
        settingsTransition.duration = 0.3
        settingsTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        settingsTransition.type = kCATransitionPush
        settingsTransition.subtype = kCATransitionFromRight
        view.window?.layer.add(settingsTransition, forKey: nil)

        navigationController?.show(vc, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToDigest" {
            let settingsTransition = CATransition()
            settingsTransition.duration = 0.3
            settingsTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            settingsTransition.type = kCATransitionPush
            settingsTransition.subtype = kCATransitionFromRight
            view.window?.layer.add(settingsTransition, forKey: nil)
        }

        if segue.identifier == "chatWithUs" {
            let vc = segue.destination as? ShareViewController
            vc?.mode = .chatWithUs
        }
    }
}

// MARK: - UITableViewDataSource
extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let font = JoyrideFonts.regularFont(ofSize: 15)

        if indexPath.row == 9 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            let frame = CGRect(x: cell.frame.origin.x + 5,
                               y: cell.frame.origin.y + 15,
                               width: view.frame.size.width - 10,
                               height: cell.frame.size.height)

            let button = UIButton(frame: frame)
            let mediumFont = JoyrideFonts.mediumFont(ofSize: 15.5)
            let title = NSAttributedString(string: "Log Out", attributes: [NSFontAttributeName: mediumFont, NSForegroundColorAttributeName: UIColor.red])
            button.setAttributedTitle(title, for: .normal)

            button.layer.cornerRadius = 6
            button.layer.borderColor = UIColor.red.cgColor
            button.layer.borderWidth = 1.5
            button.backgroundColor = UIColor.clear
            button.addTarget(self, action: #selector(logOut), for: .touchUpInside)

            cell.contentView.addSubview(button)

            // Bit of a hack to hide the last cell separator
            cell.separatorInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, cell.bounds.size.width * 1.2)

            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath)

        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = .zero
        cell.layoutMargins = .zero

        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        imageView.image = #imageLiteral(resourceName: "RightArrow")
        imageView.contentMode = .scaleAspectFit

        cell.accessoryView = imageView
        cell.textLabel?.font = font
        cell.textLabel?.text = cellNames[indexPath.row]

        return cell
    }
}

// MARK: - UITableViewDelegate
extension SettingsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 59
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "accountSegue", sender: self)
        case 1:
            performSegue(withIdentifier: "pushNotificationsSegue", sender: self)
        case 2:
            performSegue(withIdentifier: "archivesSegue", sender: self)
        case 3:
            performSegue(withIdentifier: "earnPointsSegue", sender: self)
        case 4:
            performSegue(withIdentifier: "redeemPoints", sender: self)
            break
        case 5:
            performSegue(withIdentifier: "chatWithUs", sender: self)
        case 6:
            performSegue(withIdentifier: "sponsoredPostsSegue", sender: self)
        case 7:
            performSegue(withIdentifier: "privacyPolicySegue", sender: self)
        case 8:
            performSegue(withIdentifier: "termsOfServiceSegue", sender: self)
        default:
            break
        }
    }
}
