//
//  RedeemPointsViewController.swift
//  Joyride
//
//  Created by Friia, Harrison on 6/19/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class RedeemPointsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let backButton = JoyrideControls.backButton(target: self, selector: #selector(backButtonPressed(sender:)))
        navigationItem.leftBarButtonItem = backButton
    }

    func backButtonPressed(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
