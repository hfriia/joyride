//
//  PushNotificationSettingsViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 6/9/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import UserNotifications

class PushNotificationSettingsViewController: UIViewController {

    @IBOutlet weak var onCheckmark: UIImageView!
    @IBOutlet weak var offCheckmark: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let current = UNUserNotificationCenter.current()
        
        current.getNotificationSettings(completionHandler: { [weak self] (settings) in
            if settings.authorizationStatus == .notDetermined || settings.authorizationStatus == .denied {
                DispatchQueue.main.async {
                    self?.onCheckmark.isHidden = true
                }
            } else {
                DispatchQueue.main.async {
                    self?.offCheckmark.isHidden = true
                }
            }
        })

        let backButton = JoyrideControls.backButton(target: self, selector: #selector(backButtonPressed(sender:)))
        navigationItem.leftBarButtonItem = backButton
    }

    func backButtonPressed(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func onTapped(_ sender: UITapGestureRecognizer) {
        offCheckmark.isHidden = true
        onCheckmark.isHidden = false
        
        let app = UIApplication.shared
        let appDelegate = app.delegate as? AppDelegate
        
        appDelegate?.subscribeToNotifications()
    }

    @IBAction func offTapped(_ sender: UITapGestureRecognizer) {
        offCheckmark.isHidden = false
        onCheckmark.isHidden = true
        
        let app = UIApplication.shared
        
        DispatchQueue.main.async {
            app.unregisterForRemoteNotifications()
        }
    }
}
