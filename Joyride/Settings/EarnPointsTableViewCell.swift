//
//  EarnPointsTableViewCell.swift
//  Joyride
//
//  Created by Harrison Friia on 6/9/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class EarnPointsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
