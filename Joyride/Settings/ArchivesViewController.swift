//
//  ArchivesViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 6/9/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class ArchivesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let backButton = JoyrideControls.backButton(target: self, selector: #selector(backButtonPressed(sender:)))
        navigationItem.leftBarButtonItem = backButton
    }

    func backButtonPressed(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
