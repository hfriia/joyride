//
//  EarnPointsTableViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 6/9/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import MessageUI
import iRate

struct AchievementRow {
    let rowTitle: String
    let pointValue: Int
    let segue: String?
}

class EarnPointsTableViewController: UITableViewController {

    var data: [AchievementRow]?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.tintColor = UIColor.black

        tableView.delegate = self
        tableView.dataSource = self
        // Hide empty cells at the bottom
        tableView.tableFooterView = UIView(frame: .zero)

        let backButton = JoyrideControls.backButton(target: self, selector: #selector(backButtonPressed(sender:)))
        navigationItem.leftBarButtonItem = backButton
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refresh),
                                               name: .UIApplicationWillEnterForeground,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refresh()
    }
    
    func refresh() {
        setAchievementRowData()
        tableView.reloadData()
    }

    func backButtonPressed(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "shareProductIdeas" {
            let vc = segue.destination as? ShareViewController
            vc?.mode = .shareProductIdeas
        }
    }

    func showFriendReferralMessage() {
        if MFMessageComposeViewController.canSendText() {
            let messageVC = MFMessageComposeViewController()
            messageVC.delegate = self
            messageVC.messageComposeDelegate = self
            messageVC.body = "Heyy, thought you'd really enjoy \"Hot Topics\" if you don’t already. It organizes the day’s best tips and tricks in one place; by tweaking little things, it can make life slightly easier."
            present(messageVC, animated: true, completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: .UIApplicationWillEnterForeground,
                                                  object: nil)
    }

    // MARK: - Table view delegate methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 59
    }

    func setAchievementRowData() {
        data = [AchievementRow(rowTitle: "Daily Check-In", pointValue: 3, segue: nil),
                AchievementRow(rowTitle: "Share Handy Tips & Tricks", pointValue: 8, segue: nil),
                AchievementRow(rowTitle: "Activity Achievements", pointValue: 12, segue: nil),
                AchievementRow(rowTitle: "Refer a Friend", pointValue: 30, segue: nil),
                AchievementRow(rowTitle: "Empower Women Entrepreneurs", pointValue: 85, segue: nil),
                AchievementRow(rowTitle: "Write a Review", pointValue: 50, segue: nil),
                AchievementRow(rowTitle: "Product Ideas", pointValue: 10, segue: "shareProductIdeas"),
                AchievementRow(rowTitle: "Interact with Sponsored Content", pointValue: 20, segue: nil),
                AchievementRow(rowTitle: "Birthday", pointValue: 75, segue: nil),
                AchievementRow(rowTitle: "Total Points", pointValue: JoyrideUser.current()?.points ?? 0, segue: nil)]
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.row == 1 {
            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            if let vc = sb.instantiateViewController(withIdentifier: "strongerTogether") as? SubmitDigestItemViewController {
                vc.displayMode = .strongerTogether

                present(vc, animated: true, completion: nil)
            }

            return
        }

        if indexPath.row == 2 {
            // Activity Achievements
            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            if let vc = sb.instantiateViewController(withIdentifier: "strongerTogether") as? SubmitDigestItemViewController {
                vc.displayMode = .activityAchievements

                present(vc, animated: true, completion: nil)
            }

            return
        }

        if indexPath.row == 3 {
            showFriendReferralMessage()
            return
        }

        if indexPath.row == 4 {

            if let url = URL(string: "https://www.toryburchfoundation.org/donate/") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                PointsCollector.addPoints(.empowerWomen)
            }
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        if indexPath.row == 5 {
            // Write a review
            iRate.sharedInstance().promptIfNetworkAvailable()
            iRate.sharedInstance().delegate = self
        }

        guard let data = data else {
            return
        }

        let rowItem = data[indexPath.row]

        if let segue = rowItem.segue {
            performSegue(withIdentifier: segue, sender: self)
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 9 {
            
            guard let cell2 = tableView.dequeueReusableCell(withIdentifier: "totalPointsCell", for: indexPath) as? EarnPointsTableViewCell else {
                return UITableViewCell()
            }
            
            cell2.titleLabel.font = JoyrideFonts.mediumFont(ofSize: 15.5)
            cell2.pointsLabel.font = JoyrideFonts.mediumFont(ofSize: 15.5)

            // Bit of a hack to hide the last cell separator
            cell2.separatorInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, cell2.bounds.size.width * 1.2)
            
            cell2.preservesSuperviewLayoutMargins = false
            cell2.layoutMargins = .zero
            
            if let achievementRow = data?[indexPath.row] {
                cell2.titleLabel.text = achievementRow.rowTitle
                cell2.pointsLabel.text = achievementRow.pointValue.description
                cell2.titleLabel.sizeToFit()
            }
            
            return cell2
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "earnPointsCell", for: indexPath) as? EarnPointsTableViewCell else {
            return UITableViewCell()
        }
        
        if let achievementRow = data?[indexPath.row] {
            cell.titleLabel.text = achievementRow.rowTitle
            if 0...8 ~= indexPath.row {
                cell.pointsLabel.text = "+" + achievementRow.pointValue.description
            }
            cell.titleLabel.sizeToFit()
        }
        
        cell.separatorInset = .zero

        if 1...6 ~= indexPath.row {
            cell.titleLabel.textColor = UIColor.red
        }

        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = .zero

        return cell
    }
}

// MARK: - MFMessageComposeViewControllerDelegate
extension EarnPointsTableViewController: MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        if result == .sent {
            PointsCollector.addPoints(.referAFriend)
        }
        
        controller.dismiss(animated: true)
    }
}

extension EarnPointsTableViewController: iRateDelegate {
    func iRateUserDidAttemptToRateApp() {
        PointsCollector.addPoints(.writeAReview)
    }
}
