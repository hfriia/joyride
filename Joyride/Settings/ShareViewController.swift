//
//  ShareViewController.swift
//  Joyride
//
//  Created by Friia, Harrison on 6/19/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import MessageUI

enum ShareMode {
    case shareActivityAchievements
    case shareProductIdeas
    case chatWithUs
}

class ShareViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var shareTextView: UITextView!

    var mode: ShareMode!

    override func viewDidLoad() {
        super.viewDidLoad()

        let rightButton = JoyrideControls.plusButton(target: self, selector: #selector(sendMessage(_:)))
        navBar.topItem?.rightBarButtonItem = rightButton

        let backButton = JoyrideControls.backButton(target: self, selector: #selector(cancel))
        navBar.topItem?.leftBarButtonItem = backButton
        
        shareTextView.delegate = self
        shareTextView.textColor = .gray

        setUp()
    }

    func setUp() {
        guard let mode = mode else {
            return
        }

        switch mode {
        case .shareActivityAchievements:
            navBar.topItem?.title = "Activity Achievements"

        case .shareProductIdeas:
            navBar.topItem?.title = "Product Ideas"
            shareTextView.text = "share product ideas!"

        case .chatWithUs:
            navBar.topItem?.title = "Chat with Us"
            shareTextView.text = "questions or concerns?"
        }
    }

    func cancel() {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func sendMessage(_ sender: UIBarButtonItem) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            present(mailComposeViewController, animated: true, completion: nil)
        } else {
//            self.showSendMailErrorAlert()
        }
    }

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self

        mailComposerVC.setToRecipients(["team@hottopics.co"])
        mailComposerVC.setMessageBody(shareTextView.text, isHTML: false)

        guard let mode = mode else { return mailComposerVC }

        switch mode {
        case .shareActivityAchievements:
            mailComposerVC.setSubject("Activity Achievements")

        case .shareProductIdeas:
            mailComposerVC.setSubject("Product Ideas")

        case .chatWithUs:
            mailComposerVC.setSubject("Chat with Us")
        }

        return mailComposerVC
    }
}

// MARK: - UITextViewDelegate
extension ShareViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if shareTextView.text == "share product ideas!" ||
            shareTextView.text == "questions or concerns?" {
            shareTextView.text = ""
            shareTextView.textColor = UIColor.darkText
        }
    }
}

// MARK: - MFMessageComposeViewControllerDelegate
extension ShareViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        if result == .sent {
            guard let mode = mode else {
                return
            }
            switch mode {
            case .shareActivityAchievements:
                PointsCollector.addPoints(.activityAchievements)
            case .shareProductIdeas:
                PointsCollector.addPoints(.productIdeas)
            default:
                break
            }
        }
        
        controller.dismiss(animated: true) { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
    }
}
