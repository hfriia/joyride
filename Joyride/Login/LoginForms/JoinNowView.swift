//
//  JoinNowView.swift
//  Joyride
//
//  Created by Harrison Friia on 4/10/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import Parse

protocol JoinNowViewDelegate: class {
    func didFinishSignUp(user: JoyrideUser)
    func showImagePicker(picker: UIImagePickerController)
    func showTermsOfService()
    func showPrivacyPolicy()
}

class JoinNowView: UIView {

    @IBOutlet weak var nameTextField: LoginFormTextField!
//    @IBOutlet weak var birthdayTextField: LoginFormTextField!
    @IBOutlet weak var emailTextField: LoginFormTextField!
    @IBOutlet weak var passwordTextField: LoginFormTextField!
//    @IBOutlet weak var profilePhotoButton: UIButton!
    @IBOutlet weak var joinNowButton: UIButton!
    @IBOutlet weak var disclaimerTextButton: UIButton!
    @IBOutlet weak var privacyPolicyButton: UIButton!

    @IBOutlet var view: UIView!
    
    var profilePhoto: UIImage?

    lazy var imagePicker = UIImagePickerController()
    weak var delegate: JoinNowViewDelegate?

    let pickerData = ["Yes", "No"]

    override init(frame: CGRect) {
        super.init(frame: frame)

        Bundle.main.loadNibNamed("JoinNowView", owner: self, options: nil)
        view.frame = frame
        addSubview(self.view)
        imagePicker.delegate = self
        nameTextField.becomeFirstResponder()

        self.setUpUI()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    func setUpUI() {
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue] as [String : Any]
        let fontAndColorAttributes = [NSFontAttributeName: JoyrideFonts.thinFont(ofSize: 10.5), NSForegroundColorAttributeName: UIColor.darkText]
        let mutableAttributedString = NSMutableAttributedString(string: "By joining you agree to our Terms of Service &")
        
        let privacyPolicyString = NSMutableAttributedString(string: "Privacy Policy")

        let underlineRange1 = NSRange(location: 28, length: 16)
        
        let fullRange = NSRange(location: 0, length: 46)
        let privacyPolicyRange = NSRange(location: 0, length: 14)

        mutableAttributedString.addAttributes(fontAndColorAttributes, range: fullRange)
        mutableAttributedString.addAttributes(underlineAttribute, range: underlineRange1)
        
        privacyPolicyString.addAttributes(fontAndColorAttributes, range: privacyPolicyRange)
        privacyPolicyString.addAttributes(underlineAttribute, range: privacyPolicyRange)

        disclaimerTextButton.setAttributedTitle(mutableAttributedString, for: .normal)
        privacyPolicyButton.setAttributedTitle(privacyPolicyString, for: .normal)

        let placeholderAttributes = [NSFontAttributeName: JoyrideFonts.regularFont(ofSize: 15), NSForegroundColorAttributeName: UIColor.gray]

        nameTextField.delegate = self
        nameTextField.tag = 0
        nameTextField.attributedPlaceholder = NSAttributedString(string: "Name", attributes: placeholderAttributes)

//        birthdayTextField.inputView = JoyrideUtilities.birthdayDatePicker(selector: #selector(didChangeDate(sender:)))
//        birthdayTextField.delegate = self
//        birthdayTextField.tag = 1
//        birthdayTextField.attributedPlaceholder = NSAttributedString(string: "Birthday", attributes: placeholderAttributes)

        emailTextField.delegate = self
        emailTextField.tag = 2
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: placeholderAttributes)

        passwordTextField.delegate = self
        passwordTextField.tag = 3
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: placeholderAttributes)

//        profilePhotoButton.layer.cornerRadius = 8
//        profilePhotoButton.layer.borderWidth = 1
//        profilePhotoButton.layer.borderColor = UIColor.lightGray.cgColor
//        profilePhotoButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
//        profilePhotoButton.setAttributedTitle(NSAttributedString(string: "Profile Photo (Optional)", attributes: placeholderAttributes), for: .normal)

        joinNowButton.layer.cornerRadius = 8
        joinNowButton.layer.borderColor = UIColor.red.cgColor
        joinNowButton.layer.borderWidth = 1.5
    }

    func didChangeDate(sender: UIDatePicker) {
//        birthdayTextField.text = sender.date.birthdayDateString
    }

    @IBAction func joinNow(_ sender: UIButton) {
        checkFormCompletion()
    }

    @IBAction func showTermsOfService(_ sender: UIButton) {
        delegate?.showTermsOfService()
    }
    
    @IBAction func showPrivacyPolicy(_ sender: Any) {
        delegate?.showPrivacyPolicy()
    }

    func checkFormCompletion() {
        var complete = true

        emailTextField.text = EmailTextFieldValidator().validate(field: emailTextField)

        let fieldArray = [nameTextField, emailTextField, passwordTextField]

        for field in fieldArray {
            if field?.text == "" {

                field?.becomeFirstResponder()

                complete = false
            }
        }

        if complete {
            if let user = generateUser() {
                delegate?.didFinishSignUp(user: user)
            }
        }
    }

    @IBAction func chooseProfilePhoto(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary

        guard let types = UIImagePickerController.availableMediaTypes(for: .photoLibrary) else {
            return
        }

        imagePicker.mediaTypes = types

        delegate?.showImagePicker(picker: imagePicker)
    }

//    func setProfilePhoto(image: UIImage) {
//        profilePhotoButton.imageView?.contentMode = .scaleAspectFit
//        profilePhotoButton.setImage(#imageLiteral(resourceName: "SettingsCheckmark"), for: .normal)
//        profilePhotoButton.tintColor = UIColor.red
//
//        profilePhoto = image
//    }

    func generateUser() -> JoyrideUser? {
        guard let name = nameTextField.text,
//            validDate(birthdayTextField.text),
//            let birthday = birthdayTextField.text,
//            let birthdayDate = DateFormatter.birthdayFormatter.date(from: birthday),
            let email = emailTextField.text,
            let password = passwordTextField.text else {
            return nil
        }
        
        var imageFile: PFFile?
        if let image = profilePhoto,
            let data = UIImagePNGRepresentation(image),
            let file = PFFile(data: data) {
            imageFile = file
        }
        
        if imageFile == nil,
            let defaultData = UIImagePNGRepresentation(#imageLiteral(resourceName: "user_profile_female.jpg")),
            let file = PFFile(data: defaultData) {
            imageFile = file
        }
        
        guard let validImageFile = imageFile else { return nil }

        let user = JoyrideUser(name: name,
                               birthday: nil,
                               email: email,
                               password: password,
                               profileImageFile: nil,
                               points: 3, // Give them 3 points for daily check in
                               lastLoginDate: Date(),
                               bookmarks: [String]())

        return user
    }
    
    func validDate(_ date: String?) -> Bool {
        // Make sure we can turn this string into a birthday-formatted date
        guard let date = date, let _ = DateFormatter.birthdayFormatter.date(from: date) else {
            return false
        }
        
        return true
    }

    func clearForm() {
        nameTextField.text = nil
//        birthdayTextField.text = nil
        emailTextField.text = nil
        passwordTextField.text = nil
    }
}

extension JoinNowView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        let nextTag = textField.tag + 1

        // Try to find next responder
        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder!

        if (nextResponder != nil) {
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        } else {

            checkFormCompletion()
        }

        // We do not want UITextField to insert line-breaks.
        return false
    }
}

extension JoinNowView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//
//        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
//
//            setProfilePhoto(image: image)
//            imagePicker.dismiss(animated: true, completion: nil)
//
//        } else {
//            print("Something went wrong")
//        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
