//
//  LoginFormViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 4/10/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import Parse

enum DisplayMode {
    case joinNow
    case resetPassword
    case logIn
}

protocol LoginFormViewControllerDelegate: class {
    func readyForDismissal()
    func logIn()
    func forgotPassword()
}

class LoginFormViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    var mode: DisplayMode!

    weak var delegate: LoginFormViewControllerDelegate?
    var joinNowView: JoinNowView?
    var logInView: LogInView?
    var resetPasswordView: ResetPasswordView?

    var pickedPhoto = false

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // When coming back from image picker this brings the keyboard back
        joinNowView?.nameTextField.becomeFirstResponder()

        guard let parent = self.parent as? LoginViewController else {
            return
        }

        let width = parent.loginFormContainerView.frame.size.width

        switch mode! {
        case .joinNow:
            showJoinNowView(width: width)
        case .logIn:
            showLogInView(width: width)
        case .resetPassword:
            showResetPasswordView(width: width)
        }
    }

    func showJoinNowView(width: CGFloat) {
        let joinNowViewFrame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: width, height: 333)

        if joinNowView == nil {
            joinNowView = JoinNowView.init(frame: joinNowViewFrame)
        }

        guard let joinNowView = joinNowView else { return }

        joinNowView.delegate = self
        scrollView.contentSize = joinNowView.frame.size

        scrollView.addSubview(joinNowView)
    }

    func showLogInView(width: CGFloat) {
        let logInViewFrame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: width, height: 182)

        logInView = LogInView.init(frame: logInViewFrame)

        guard let logInView = logInView else { return }

        logInView.delegate = self
        scrollView.contentSize = logInView.frame.size
        scrollView.addSubview(logInView)
    }

    func showResetPasswordView(width: CGFloat) {
        let resetPasswordViewFrame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: width, height: 111)

        resetPasswordView = ResetPasswordView.init(frame: resetPasswordViewFrame)

        guard let resetPasswordView = resetPasswordView else { return }

        scrollView.contentSize = resetPasswordView.frame.size

        scrollView.addSubview(resetPasswordView)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "digestSegue" {
            // Dismiss the keyboard
            scrollView.subviews.first!.endEditing(true)
        }
    }
    
    func showAlertForError(_ error: Error) {
        let errorVC = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        errorVC.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            errorVC.dismiss(animated: true, completion: nil)
        }))
        self.present(errorVC, animated: true, completion: nil)
    }
}

// MARK: JoinNowViewDelegate
extension LoginFormViewController: JoinNowViewDelegate {

    func didFinishSignUp(user: JoyrideUser) {
        user.signUpInBackground { [weak self] (success, error) in
            guard let strongSelf = self else { return }
            if let error = error {
                strongSelf.showAlertForError(error)
            } else {
                strongSelf.delegate?.readyForDismissal()
            }
        }
    }

    func showImagePicker(picker: UIImagePickerController) {
        present(picker, animated: true, completion: nil)
    }

    func showTermsOfService() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = sb.instantiateViewController(withIdentifier: "disclaimerVC") as? DisclaimerViewController else {
            return
        }

        present(vc, animated: true, completion: nil)
    }
    
    func showPrivacyPolicy() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = sb.instantiateViewController(withIdentifier: "privacyPolicyVC") as? LoginPrivacyPolicyViewController else {
            return
        }
        
        present(vc, animated: true, completion: nil)
    }
}

// MARK:- LogInViewDelegate
extension LoginFormViewController: LogInViewDelegate {

    func logIn(email: String, password: String) {
        logInView?.logInButton.isUserInteractionEnabled = false

        JoyrideUser.logInWithUsername(inBackground: email, password: password) { [weak self] (user, error) in
            guard let strongSelf = self else { return }

            strongSelf.logInView?.logInButton.isUserInteractionEnabled = true

            if let error = error {
                strongSelf.showAlertForError(error)
            }

            if user != nil {
                strongSelf.delegate?.logIn()
            }
        }
        
//        delegate?.logIn()
    }

    func forgotPassword() {
        delegate?.forgotPassword()
    }
}
