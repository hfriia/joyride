//
//  ResetPasswordView.swift
//  Joyride
//
//  Created by Harrison Friia on 6/15/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class ResetPasswordView: UIView {

    @IBOutlet weak var emailTextField: LoginFormTextField!
    @IBOutlet weak var resetPasswordButton: UIButton!

    @IBOutlet var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)

        Bundle.main.loadNibNamed("ResetPasswordView", owner: self, options: nil)
        view.frame = frame
        addSubview(self.view)

        emailTextField.becomeFirstResponder()
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSFontAttributeName: JoyrideFonts.regularFont(ofSize: 15), NSForegroundColorAttributeName: UIColor.gray])

        setUpUI()
    }

    func setUpUI() {
        resetPasswordButton.layer.cornerRadius = 8
        resetPasswordButton.layer.borderColor = UIColor.red.cgColor
        resetPasswordButton.layer.borderWidth = 1.5
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
}
