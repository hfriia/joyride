//
//  LogInView.swift
//  Joyride
//
//  Created by Harrison Friia on 6/15/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

protocol LogInViewDelegate: class {
    func logIn(email: String, password: String)
    func forgotPassword()
}

class LogInView: UIView {

    @IBOutlet weak var emailTextField: LoginFormTextField!
    @IBOutlet weak var passwordTextField: LoginFormTextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!

    @IBOutlet var view: UIView!

    weak var delegate: LogInViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)

        Bundle.main.loadNibNamed("LogInView", owner: self, options: nil)
        view.frame = frame
        addSubview(self.view)

        emailTextField.becomeFirstResponder()

        setUpUI()
    }

    func setUpUI() {
        let attributes = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue, NSFontAttributeName: JoyrideFonts.thinFont(ofSize: 10.5), NSForegroundColorAttributeName: UIColor.darkText] as [String : Any]
        let underlineAttributedString = NSAttributedString(string: "Forgot your password?", attributes: attributes)
        forgotPasswordButton.setAttributedTitle(underlineAttributedString, for: .normal)

        let placeholderAttributes = [NSFontAttributeName: JoyrideFonts.regularFont(ofSize: 15), NSForegroundColorAttributeName: UIColor.gray]

        emailTextField.delegate = self
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: placeholderAttributes)

        passwordTextField.delegate = self
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: placeholderAttributes)

        logInButton.layer.cornerRadius = 8
        logInButton.layer.borderColor = UIColor.red.cgColor
        logInButton.layer.borderWidth = 1.5
    }

    func checkFormCompletion() {
        var complete = true

        let fieldArray = [emailTextField, passwordTextField]

        for field in fieldArray {
            if field?.text == "" {

                field?.becomeFirstResponder()

                complete = false
            }
        }

        guard let email = emailTextField.text,
            let password = passwordTextField.text else {
                return
        }

        if complete {
            delegate?.logIn(email: email, password: password)
        }
    }

    @IBAction func logIn(_ sender: Any) {
        checkFormCompletion()
//        delegate?.logIn(email: "", password: "")
    }

    @IBAction func forgotPassword(_ sender: UIButton) {
        delegate?.forgotPassword()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
}

extension LogInView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        let nextTag = textField.tag + 1

        // Try to find next responder
        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder!

        if (nextResponder != nil) {
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard

            checkFormCompletion()

        }

        return false // We do not want UITextField to insert line-breaks.
    }
}
