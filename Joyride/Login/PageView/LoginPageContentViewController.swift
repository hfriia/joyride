//
//  LoginPageContentViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 2/26/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class LoginPageContentViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!

    var pageIndex: Int!
    var imageName: String!
    var text: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        let image = UIImage(named: imageName)

        imageView.image = image
        textLabel.text = text
    }
}
