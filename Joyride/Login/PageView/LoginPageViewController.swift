//
//  LoginPageViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 2/26/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

protocol LoginPageViewControllerDelegate: class {
    /**
     Called when the number of pages is updated.
     
     - parameter loginPageViewController: the LoginPageViewController instance
     - parameter count: the total number of pages.
     */
    func loginPageViewController(_: LoginPageViewController,
                                    didUpdatePageCount count: Int)

    /**
     Called when the current index is updated.
     
     - parameter loginPageViewController: the LoginPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func loginPageViewController(_: LoginPageViewController,
                                    didUpdatePageIndex index: Int)
}

class LoginPageViewController: UIPageViewController {

    weak var loginDelegate: LoginPageViewControllerDelegate?
    let imageNames = ["LoginImage2.jpg"]

    let imageText = ["Don't worry; starting today we'll organize the day's best tips and tricks in one place."]

    override func viewDidLoad() {
        super.viewDidLoad()

        delegate = self
        dataSource = self

        guard let startViewController = viewControllerAtIndex(index: 0) else {
            return
        }

        loginDelegate?.loginPageViewController(self, didUpdatePageCount: imageNames.count)

        setViewControllers([startViewController],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
}

// MARK: - Page View Controller Delegate
extension LoginPageViewController: UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {

        if let firstViewController = viewControllers?.first as? LoginPageContentViewController,
            let index = imageNames.index(of: firstViewController.imageName) {
            loginDelegate?.loginPageViewController(self, didUpdatePageIndex: index)
        }
    }
}

// MARK: - Page View Controller Data Source
extension LoginPageViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let vc = viewController as? LoginPageContentViewController else {
            return nil
        }

        if ((vc.pageIndex == 0) || (vc.pageIndex == NSNotFound)) {
            return nil
        }

        return viewControllerAtIndex(index: vc.pageIndex - 1)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let vc = viewController as? LoginPageContentViewController else {
            return nil
        }

        if (vc.pageIndex == NSNotFound) {
            return nil
        }

        return viewControllerAtIndex(index: vc.pageIndex + 1)
    }
}

// MARK: - Private helper methods
private extension LoginPageViewController {

    func viewControllerAtIndex(index: Int) -> LoginPageContentViewController? {
        let contentViewController = storyboard?.instantiateViewController(withIdentifier: "LoginPageContentViewController") as? LoginPageContentViewController

        if index >= imageNames.count || imageNames.count == 0 {
            return nil
        }

        contentViewController?.pageIndex = index
        contentViewController?.imageName = imageNames[index]
        contentViewController?.text = imageText[index]

        return contentViewController
    }
}
