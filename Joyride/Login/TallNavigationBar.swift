//
//  TallNavigationBar.swift
//  Joyride
//
//  Created by Friia, Harrison on 6/19/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class TallNavigationBar: UINavigationBar {

    override init(frame: CGRect) {
        super.init(frame: frame)
        

    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//        clipsToBounds = false
        
//        for subview in self.subviews {
//            let stringFromClass = NSStringFromClass(subview.classForCoder)
//            print("--------- \(stringFromClass)")
//            if stringFromClass.contains("BarBackground") {
//
//                var newFrame = subview.frame
//                newFrame.origin.y = 0
//                newFrame.size.height = 60
//
//                subview.frame = newFrame
//
//            } else if stringFromClass.contains("UINavigationBarContentView") {
//                var newFrame = subview.frame
//                newFrame.origin.y = 7
//                newFrame.size.height = 44
//
//                subview.frame = newFrame
//            }
//        }
//    }

//    override func sizeThatFits(_ size: CGSize) -> CGSize {
//        var size = super.sizeThatFits(size)
//        size.height = 64
//        return size
//    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.titleTextAttributes = [NSFontAttributeName: JoyrideFonts.boldFont(ofSize: 20), NSForegroundColorAttributeName: UIColor.red]
    }
}
