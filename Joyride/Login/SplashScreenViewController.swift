//
//  SplashScreenViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 6/16/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import Parse

class SplashScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(moveToLogin), userInfo: nil, repeats: false)
    }

    func moveToLogin() {
//        // We have a current, logged in user so go straight to the digest
//        if JoyrideUser.current() != nil {
//            performSegue(withIdentifier: "digestSegue", sender: self)
//            return
//        }
//
//        // User needs to log in
//        performSegue(withIdentifier: "showLogin", sender: self)
        
        performSegue(withIdentifier: "digestSegue", sender: self)
    }
}
