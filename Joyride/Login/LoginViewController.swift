//
//  LoginViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 2/26/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var pageContainerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var joinNowButton: UIButton!
    @IBOutlet weak var loginFormContainerView: UIView!

    @IBOutlet weak var loginFormContainerViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var loginFormContainerBottomConstraint: NSLayoutConstraint!

    weak var loginFormViewController: LoginFormViewController?
    var loginFormVisible = false
    var invisibleView: UIView?

    let pageVCSegue = "pageVCEmbedSegue"
    let digestSegue = "digestSegue"

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 120.0)

        logInButton.layer.cornerRadius = 8
        logInButton.layer.borderColor = UIColor.lightGray.cgColor
        logInButton.layer.borderWidth = 0.25

        joinNowButton.layer.cornerRadius = 8
        joinNowButton.layer.borderColor = UIColor.lightGray.cgColor
        joinNowButton.layer.borderWidth = 0.25
        
//        pageControl.isHidden = true

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: .UIKeyboardWillShow,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: .UIKeyboardWillHide,
                                               object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == pageVCSegue {
            let loginPageViewController = segue.destination as? LoginPageViewController
            loginPageViewController?.loginDelegate = self
            
        } else if segue.identifier == digestSegue {
            hideLoginForm()
        }
    }

    func handleTap(_ sender: UITapGestureRecognizer) {
        hideLoginForm()
    }

    @IBAction func joinNowPressed(_ sender: Any) {

        if !loginFormVisible {
            showFormWithMode(.joinNow)
        } else {
            hideLoginForm()
        }

    }

    @IBAction func logInPressed(_ sender: Any) {
        if !loginFormVisible {
            showFormWithMode(.logIn)
        } else {
            hideLoginForm()
        }
    }

    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
    }
}

// MARK: - Login form
extension LoginViewController {

    func showFormWithMode(_ mode: DisplayMode) {
        invisibleView = UIView(frame: self.view.frame)
        guard let invisibleView = invisibleView else { return }

        invisibleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(_:))))

        view.insertSubview(invisibleView, belowSubview: loginFormContainerView)

        loginFormViewController = storyboard?.instantiateViewController(withIdentifier: "LoginFormViewController") as? LoginFormViewController

        guard let controller = loginFormViewController else {
            return
        }

        controller.delegate = self
        controller.mode = mode

        controller.view.translatesAutoresizingMaskIntoConstraints = false

        switch mode {
        case .joinNow:
            loginFormContainerViewHeightContraint.constant = 233
        case .logIn:
            loginFormContainerViewHeightContraint.constant = 182
        case .resetPassword:
            loginFormContainerViewHeightContraint.constant = 111
        }

        addChildViewController(controller)
        loginFormContainerView.addSubview(controller.view)
        controller.didMove(toParentViewController: self)

        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: loginFormContainerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: loginFormContainerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: loginFormContainerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: loginFormContainerView.bottomAnchor)
            ])

        loginFormContainerView.isHidden = false
        loginFormVisible = true
    }

    func hideLoginForm() {
        invisibleView?.removeFromSuperview()

        loginFormViewController?.willMove(toParentViewController: nil)
        loginFormViewController?.view.removeFromSuperview()
        loginFormViewController?.removeFromParentViewController()

        loginFormContainerView.isHidden = true

        loginFormVisible = false
    }

    func keyboardWillShow(_ notification: NSNotification) {
        moveView(notification: notification, up: true)
    }

    func keyboardWillHide(_ notification: NSNotification) {
        moveView(notification: notification, up: false)
    }

    func moveView(notification: NSNotification, up: Bool) {

        guard let userInfo = notification.userInfo as? [String:AnyObject] else {
            return
        }

        var height = CGFloat(0)

        if up {
            if let hasHeight = heightFromUserInfo(userInfo) {
                height = hasHeight
            } else {
                return
            }
        }

        animateView(loginFormContainerView!, userInfo: userInfo, toHeight: height)
    }

    func heightFromUserInfo(_ userInfo: [String:AnyObject]) -> CGFloat? {
        guard let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect else {
            return nil
        }

        return keyboardFrame.size.height
    }

    func animateView(_ view: UIView, userInfo: [String:AnyObject], toHeight height: CGFloat) {
        guard let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
            let animationCurveInt = (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue else {
                return
        }

        let animationCurve = UIViewAnimationOptions(rawValue: animationCurveInt)

        view.layoutIfNeeded()
        loginFormContainerBottomConstraint.constant = height
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: animationCurve,
                       animations: {
                        self.view.layoutIfNeeded()
        },
                       completion: nil)
    }
}

// MARK: - LoginFormViewController Delegate
extension LoginViewController: LoginFormViewControllerDelegate {
    func readyForDismissal() {
        hideLoginForm()
        performSegue(withIdentifier: digestSegue, sender: self)
    }

    func logIn() {
        performSegue(withIdentifier: digestSegue, sender: self)
    }

    func forgotPassword() {
        hideLoginForm()
        showFormWithMode(.resetPassword)
    }
}

// MARK: - Page View Controller Delegate
extension LoginViewController: LoginPageViewControllerDelegate {
    func loginPageViewController(_ loginPageViewController: LoginPageViewController, didUpdatePageCount count: Int) {

        pageControl.numberOfPages = count
    }

    func loginPageViewController(_ loginPageViewController: LoginPageViewController, didUpdatePageIndex index: Int) {

        pageControl.currentPage = index
    }
}
