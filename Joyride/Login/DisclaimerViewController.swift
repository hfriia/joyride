//
//  DisclaimerViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 7/19/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit

class DisclaimerViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        navBar.titleTextAttributes = [NSFontAttributeName: JoyrideFonts.boldFont(ofSize: 20), NSForegroundColorAttributeName: UIColor.red]

        let backButton = JoyrideControls.backButton(target: self, selector: #selector(backButtonPressed(sender:)))
        navItem.leftBarButtonItem = backButton
    }

    func backButtonPressed(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
