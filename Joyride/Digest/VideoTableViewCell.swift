//
//  VideoTableViewCell.swift
//  Joyride
//
//  Created by Harrison Friia on 7/17/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MGSwipeTableCell

class VideoTableViewCell: MGSwipeTableCell {

    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var videoView: YTPlayerView!

    var viewModel: VideoTableCellViewModel! {
        didSet {
            configure()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        authorImageView.image = nil
    }

    private func configure() {
        viewModel.authorImageFile?.getDataInBackground { [weak self] (data, error) in
            if let data = data {
                self?.authorImageView.image = UIImage(data: data)
            }
        }
        
        authorNameLabel.text = viewModel.authorName
        titleLabel.text = viewModel.titleText
        bodyLabel.text = viewModel.bodyText
        
        if let url = viewModel.videoURL?.absoluteString {
            let urlString: NSString = NSString(string: url)
            let idString = urlString.substring(with: NSRange(location: urlString.length - 11, length: 11))
            
            videoView.load(withVideoId: idString)
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {

        }
    }

    func openVideo() {
        if let url = viewModel.videoURL {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
