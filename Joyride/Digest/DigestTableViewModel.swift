//
//  DigestItemTableViewModel.swift
//  Joyride
//
//  Created by Harrison Friia on 7/8/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import Foundation
import Parse

protocol DigestTableViewModelDelegate: class {
    func digestItemsAvailable(_ items: [DigestItem])
    func networkErrorReturned(_ error: Error)
}

class DigestTableViewModel: NSObject {

    var date: String {
        let now = Date()
        let suffix = daySuffix(from: now)
        let formatter = DateFormatter()

        formatter.locale = Locale.current
        formatter.dateFormat = "MMMM d"

        return formatter.string(from: now) + suffix
    }

    var numberOfRowsInSection: Int {
        return digestItems.count
    }

    let numberOfSections = 1

    var digestItems = [DigestItem]() {
        didSet {
            delegate?.digestItemsAvailable(digestItems)
        }
    }
    
    weak var delegate: DigestTableViewModelDelegate?

    override init() {
        super.init()
        
        retrieveDigestItems()
    }
    
    func refresh() {
        retrieveDigestItems()
    }

    func viewModelForCell(at index: Int) -> Any? {
        guard index < digestItems.count else { return nil }

        if let articleItem = digestItems[index] as? ArticleDigestItem {
            return ArticleTableCellViewModel(digestItem: articleItem)
        }

        if let videoItem = digestItems[index] as? VideoDigestItem {
            return VideoTableCellViewModel(digestItem: videoItem)
        }

        return nil
    }

    func bookmarkItem(at index: Int) {
        guard index < digestItems.count,
            let user = JoyrideUser.current() else {
            return
        }

        var items = user.bookmarks

        if let articleItem = digestItems[index] as? ArticleDigestItem,
            let articleId = articleItem.objectId {

            if items.filter({ $0 == articleId }).count > 0 { return }

            items.append(articleId)
        }

        if let videoItem = digestItems[index] as? VideoDigestItem,
            let videoId = videoItem.objectId  {

            if items.filter({ $0 == videoId }).count > 0 { return }

            items.append(videoId)
        }
        
        user.bookmarks = items
        user.saveInBackground()
    }
}

// MARK: - Private methods
private extension DigestTableViewModel {
    func retrieveDigestItems() {
        
        var tempItems = [DigestItem]()
        var lastError: Error?
        
        let queue = DispatchQueue(label: "com.joyride.joyride.digestItemRequest", attributes: .concurrent)
        let group = DispatchGroup()

        group.enter()
        queue.async(group: group) {
            self.getVideoDigestItems(completion: { (items, error) in
                if let items = items, error == nil {
                    tempItems += items
                }
                lastError = error
                group.leave()
            })
        }
        
        group.enter()
        queue.async(group: group) {
            self.getArticleDigestItems(completion: { (items, error) in
                if let items = items, error == nil {
                    tempItems += items
                }
                lastError = error
                group.leave()
            })
        }
        
        group.notify(queue: queue) { [weak self] in
            group.wait()
            DispatchQueue.main.async {
                
                if let lastError = lastError, tempItems.count == 0 {
                    self?.delegate?.networkErrorReturned(lastError)
                }
                
                self?.digestItems = tempItems.sorted { $0.position < $1.position }
            }
        }
    }

    func getArticleDigestItems(completion: @escaping ([DigestItem]?, Error?) -> Void) {

        let query = PFQuery(className:"articleDigestItem")
        query.whereKey("approved", equalTo: true).findObjectsInBackground { (objects, error) in
            if error == nil {
                if let objects = objects {
                    
                    let items = self.toArticleDigestItems(objects)
                    completion(items, nil)
                }
            } else {
                print("Error: \(error.debugDescription)")
                completion(nil, error)
            }
        }
    }
    
    func toArticleDigestItems(_ items: [PFObject]) -> [ArticleDigestItem] {
        
        var articleItems = [ArticleDigestItem]()
        
        for item in items {
            
            guard let authorName = item["authorName"] as? String,
                let titleText = item["titleText"] as? String,
                let bodyText = item["bodyText"] as? String,
                let position = item["position"] as? Int,
                let authorImageFile = item["authorImageFile"] as? PFFile else {
                    continue
            }            
            
            let originText = item["originText"] as? String
            let originName = item["originName"] as? String
            let originLink = item["originLink"] as? String
            let approved = item["approved"] as? Bool ?? false
            
            let extraImageFile = item["extraImage"] as? PFFile

            let articleItem = ArticleDigestItem(authorImageFile: authorImageFile,
                                                authorName: authorName,
                                                titleText: titleText,
                                                bodyText: bodyText,
                                                position: position,
                                                approved: approved,
                                                originText: originText,
                                                originName: originName,
                                                originLink: originLink,
                                                extraImageFile: extraImageFile)
            
            articleItem.objectId = item.objectId
            
            articleItems.append(articleItem)
        }
        
        return articleItems
    }
    
    func getVideoDigestItems(completion: @escaping ([DigestItem]?, Error?) -> Void) {
        
        let query = PFQuery(className:"videoDigestItem")
        query.whereKey("approved", equalTo: true).findObjectsInBackground { (objects, error) in
            if error == nil {
                if let objects = objects {
                    // For some reason casting the array to [VideoDigestItem] doesn't work,
                    // so map it manually with this method
                    let items = self.toVideoDigestItems(objects)
                    completion(items, nil)
                }
            } else {
                print("Error: \(error.debugDescription)")
                completion(nil, error)
            }
        }
    }
    
    func toVideoDigestItems(_ items: [PFObject]) -> [VideoDigestItem] {
        
        var videoItems = [VideoDigestItem]()
        
        for item in items {
            
            guard let authorName = item["authorName"] as? String,
                let titleText = item["titleText"] as? String,
                let bodyText = item["bodyText"] as? String,
                let position = item["position"] as? Int,
                let videoURLString = item["videoURL"] as? String,
                let imageFile = item["authorImage"] as? PFFile else {
                    break
            }
            
            let approved = item["approved"] as? Bool ?? false

            let videoItem = VideoDigestItem(authorImageFile: imageFile,
                            authorName: authorName,
                            titleText: titleText,
                            bodyText: bodyText,
                            position: position,
                            approved: approved,
                            videoURL: videoURLString)
            
            videoItem.objectId = item.objectId
            
            videoItems.append(videoItem)
        }
        
        return videoItems
    }

    func daySuffix(from date: Date) -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31:
            return "st"
        case 2, 22:
            return "nd"
        case 3, 23:
            return "rd"
        default:
            return "th"
        }
    }

//    func testDigestData() -> [DigestItem] {
//
//        let item1 = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Chriselle Lim (7).png"),
//                                      authorName: "Chriselle Lim",
//                                      titleText: "Grab & Go",
//                                      bodyText: "Listening to podcasts is one of the best ways to absorb information on the go. There are so many different podcasts out there, but I listen to a variety that are both inspiring and entertaining! Here is my list of podcasts that you need in your life! Radiolab, Dear Sugar, Stuff Mom Never Told You, The Style Scout, & Serial.",
//                                      originText: "Read the original article on",
//                                      originName: "The Chriselle Factor",
//                                      originLink: URL(string: "http://thechrisellefactor.com/2016/03/9-podcasts-you-need-to-know/")!,
//                                      extraImage: nil)
//
//        let niloHaq = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "NiloHaq.png"),
//                                      authorName: "Nilo Haq",
//                                      titleText: "Beauty Trick",
//                                      bodyText: "Vaseline can help the scent of your perfume last for a longer time. Just add some vaseline to your wrists and neck before spraying perfume. I also love using it on the lips! Apply it on the lips and then dab on any powder eyeshadow or blush on top to create a lip stain! Lips remain hydrated and have a natural tint of color.",
//                                      originText: "Read the original article on",
//                                      originName: "Saudi Beauty Blog",
//                                      originLink: URL(string: "http://saudibeautyblog.com/6-ways-to-use-vaseline-in-your-beauty-rituals/")!,
//                                      extraImage: nil)
//
//        let keyairaBoone = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Keyaira Boone.png"),
//                                      authorName: "Keyaira Boone",
//                                      titleText: "Warning!",
//                                      bodyText: "All too often we buy out of habit and not out of understanding. I’m not saying don’t buy things. But you should ask yourself questions before you buy. Questions you should ask: “Do I really need this?”, “What purpose will it serve?”, and “Am I buying this to appeal to others or is it a legitimate investment in my goals?”. These questions are helpful in combating unnecessary purchases.",
//                                      originText: "Read the original article on",
//                                      originName: "Love Brown Sugar",
//                                      originLink: URL(string:"http://www.lovebrownsugar.com/3-tips-for-fab-finances-in-2017/")!,
//                                      extraImage: nil)
//
//        let suePressey = VideoDigestItem(authorImage: #imageLiteral(resourceName: "Sue Pressey.png"),
//                                     authorName: "Sue Pressey",
//                                     titleText: "Stumped For Dinner?",
//                                     bodyText: "Bibimbap is a fantastic gateway Korean dish if you haven't delved into this bulgogi-flavored world before. It's essentially rice cooked in a hot stone pot, filled with a bunch of garnishes, and usually topped with a raw egg and some chili sauce.",
//                                     videoURL: URL(string:"https://www.youtube.com/watch?v=drHc1f-LrpI")!)
//        
//        let rentTheRunway = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "jyzgTnJs.jpg"),
//                                            authorName: "Rent The Runway",
//                                            titleText: "Game Changer",
//                                            bodyText: "Why buy when you can borrow? Subscribe to unlimited for $139/month; you'll receive designer dresses, accessories, tops, skirts and more on rotation. Don't worry, everything is insured. Send 1, 2 or 3 items back and select the same number for your next shipment.",
//                                            originText: "",
//                                            originName: "Join Today!",
//                                            originLink: URL(string:"https://www.renttherunway.com/")!,
//                                            extraImage: nil)
//
//        let item5 = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Gabrielle Union.png"),
//                                      authorName: "Gabrielle Union",
//                                      titleText: "#Inner Freak",
//                                      bodyText: "Sometimes my girlfriends will mention sexting or certain positions as something they'd do only with a guy on spring break. They'll say, \"You don't do that with someone you love.\" And I'll say, \"That's who you're supposed to do it with.\" If I can do this awesome, amazing thing with some dude I met at a bar, why wouldn't I be able to do it with the person I love? If you're into it and he's into it and it's legal enough that the police aren't going be involved—then go for it! Do it. A lot.",
//                                      originText: "Read the original article on",
//                                      originName: "Glamour",
//                                      originLink: URL(string:"http://www.glamour.com/story/gabrielle-union-dos-and-donts")!,
//                                      extraImage: nil)
//
//        let laurenHefez = VideoDigestItem(authorImage: #imageLiteral(resourceName: "Lauren Hefez.png"),
//                                     authorName: "Lauren Hefez",
//                                     titleText: "Abs-Abs-Abs",
//                                     bodyText: "Who doesn't have 5 minutes? Grab a mat, and a towel or yoga block and workout with me and get your tummy toned. We will feel the burn together!",
//                                     videoURL: URL(string:"https://www.youtube.com/watch?v=dLJi-P68JGU")!)
//
//        let jenniferHyman = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Hyman_Jennifer.jpg"),
//                                      authorName: "Jennifer Hyman",
//                                      titleText: "Be Happier At Work",
//                                      bodyText: "In order to be a normal, positive person in the office, do not let issues consume you throughout the night. It used to be that I'd take an issue home with me and I'd obsess about it for the ten hours until I had to be at work again. Not only is that not going to not accomplish anything, it's actually going to make the next day worse.",
//                                      originText: "Read the original article on",
//                                      originName: "Elle",
//                                      originLink: URL(string:"http://www.elle.com/beauty/health-fitness/news/a31207/jennifer-hyman-rent-the-runway-founder-nighttime-routine/")!,
//                                      extraImage: nil)
//
//        let item8 = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Crystal Tavares.png"),
//                                      authorName: "Crystal Tavares",
//                                      titleText: "Grab & Go",
//                                      bodyText: "No matter how early you wake up, you won't get very far without a nutrient-rich breakfast to sustain you. Combine 6 oz. milk (both dairy and non-dairy work well) 1 handful of blueberries, raspberries and/or strawberries plus 1 handful of ice cubes!",
//                                      originText: nil,
//                                      originName: nil,
//                                      originLink: nil,
//                                      extraImage: #imageLiteral(resourceName: "smoothie.jpg"))
//
//        let item9 = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Tara Thompson.png"),
//                                      authorName: "Tara Thompson",
//                                      titleText: "Warning!",
//                                      bodyText: "Sadly, one of the “dirtiest” beauty product categories is nail polish, and understandably so: the ingredients that allow for that super-shiny, “vinyl-like finish” that we all love do come with a price. The good news is there are brands blazing the non-toxic nail trail—meaning that you can still have shiny, bright nails minus the most worrisome additives. Here are a few brands I love; Grand By Tenoverten, Zoya, Tea Time Nail Lacquer & Sinopia By Jin Soon.",
//                                      originText: "Read the original article on",
//                                      originName: "Camille Styles",
//                                      originLink: URL(string:"http://camillestyles.com/beauty-and-style/decoder/the-best-all-natural-nail-polish-brands/")!,
//                                      extraImage: nil)
//
//        let hithaPalepu = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Hitha Palepu.png"),
//                                      authorName: "Hitha Palepu",
//                                      titleText: "Email Sucks; It Just Does",
//                                      bodyText: "We spend about 30 hours a week on e-mail at work. Our inboxes are constantly filling up, and it’s impossible to get any productive work done while keeping up with the steady stream of requests, notifications, and sales alerts. If e-mail is taking over your life, download Spark. It's the smartest, easiest to use e-mail I’ve ever used. Their smart notifications filter out the noise, letting you know when an email is important, saving you from notification overload.",
//                                      originText: "Read the original article on",
//                                      originName: "Hitha On The Go",
//                                      originLink: URL(string:"http://www.hithaonthego.com/app-swap-spark-email/")!,
//                                      extraImage: nil)
//        
//        let wealthfront = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Wealthfront_Logo.png"),
//                                            authorName: "Wealthfront",
//                                            titleText: "Afraid To Invest?",
//                                            bodyText: "We all know that we should invest (hint: doing so allows you to grow your money at a higher interest rate than a savings account). Investing doesn't have to be intimidating, complex, or expensive, thanks to software platforms like Wealthfront.",
//                                            originText: "",
//                                            originName: "Wealthfront",
//                                            originLink: URL(string:"http://www.wealthfront.com")!,
//                                            extraImage: nil)
//
//        let emilyMorse = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Emily Morse.png"),
//                                       authorName: "Emily Morse",
//                                       titleText: "Faking Orgasms?",
//                                       bodyText: "The only thing that happens when you’re faking an orgasm is that you’re sending a mixed message to your partner. He’s thinking he’s the king of the world because you’re having an orgasm – and yet you’re not having any pleasure. In fact, one reason many women can’t orgasm during intercourse is because most sex positions don’t allow for enough clitoral stimulation. So don’t be afraid to use lubricants or your hands or add a clitoral sex toy. Really, it’s just saying, you know what? I’m not getting there and being honest about it.",
//                                       originText: "Head over to",
//                                       originName: "Sex with Emily",
//                                       originLink: URL(string:"http://sexwithemily.com/about/#.WXelWiMrIg4")!,
//                                       extraImage: nil)
//
//        let item12 = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "CamilleStyles.png"),
//                                      authorName: "Camille Styles",
//                                      titleText: "Take A Moment To Pause",
//                                      bodyText: "I used to wake up and drive headfirst into tackling my inbox & checking social media. Sure it felt productive but I found that it would cause my mind to race and I'd feel somewhat distracted all morning. Now I try to take a few minutes to pause in the morning before consuming anything digital-whether you use this time to pray, meditate, or breathe deeply, there’s no doubt that you’ll feel more centered for the rest of the day.",
//                                      originText: "Read the original article on",
//                                      originName: "Camille Styles",
//                                      originLink: URL(string: "http://camillestyles.com/wellness/life-lessons/how-to-start-the-day-on-a-happy-note")!,
//                                      extraImage: nil)
//
//        let item13 = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Sophia Bush.png"),
//                                       authorName: "Sophia Bush",
//                                       titleText: "Beauty Hack",
//                                       bodyText: "My favorite little life hack in the morning when I wash my hair, is that I’ll put all my products in it and I’ll leave it alone. I’ll go grab my breakfast, make my coffee, throw a little makeup on, get dressed and by the time I’m turning my attention back to my hair it’s almost dry. I’ll do a quick blast with a blow dryer, a little heat styling if I need it.",
//                                       originText: "Read the original article on",
//                                       originName: "Brit + Co",
//                                       originLink: URL(string: "https://www.brit.co/sophia-bush-eco-beauty-tips/")!,
//                                       extraImage: nil)
//
//        let joyBauer = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Joy Bauer.png"),
//                                       authorName: "Joy Bauer",
//                                       titleText: "Grab & Go",
//                                       bodyText: "No matter how early you wake up, you won't get very far without a nutrient-rich breakfast to sustain you. Combine 3⁄4 cup milk (skim, 1% low-fat, soy or almond), 1⁄2 a banana, a 6-ounce container nonfat plain greek yogurt, 3⁄4 cup fresh or frozen berries, and a few ice cubes in a blender. Total: 265 calories, 25 g protein, 2 g fat, 40 g carb, 40 g fiber!",
//                                       originText: "Read the original article on",
//                                       originName: "Joy Bauer",
//                                       originLink: URL(string: "http://www.joybauer.com/photo-gallery/energy-boosting-breakfasts/berry-protein-smoothie/")!,
//                                       extraImage: #imageLiteral(resourceName: "smoothie.jpg"))
//
//        let item15 = ArticleDigestItem(authorImage: #imageLiteral(resourceName: "Kelsey Clark.png"),
//                                       authorName: "Kelsey Clark",
//                                       titleText: "Rise & Shine!",
//                                       bodyText: "A gut-wrenching alarm isn't going to help you ease into your day—especially if you aren't a morning person. Consider changing your alarm tone to calming ocean waves or even one of your favorite songs to peacefully rouse you from your slumber. Scientists have proven that music makes you happier, so why not take advantage of that each and every morning? Try the Wake app to get up gently.",
//                                       originText: "Read the original article on",
//                                       originName: "Mydomaine",
//                                       originLink: URL(string: "http://www.mydomaine.com/morning-productivity-hacks")!,
//                                       extraImage: nil)
//
//        let data = [joyBauer, niloHaq, hithaPalepu, laurenHefez, emilyMorse, keyairaBoone, suePressey, jenniferHyman]
//
//        return data
//    }
}
