//
//  ArticleTableViewCell.swift
//  Joyride
//
//  Created by Harrison Friia on 7/8/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class ArticleTableViewCell: MGSwipeTableCell {

    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var extraImageView: UIImageView!
    @IBOutlet weak var originalArticleTextView: UITextView!

    @IBOutlet weak var originalArticleTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var extraImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var extraImageViewBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var extraImageViewTopSpace: NSLayoutConstraint!

    var viewModel: ArticleTableCellViewModel! {
        didSet {
            configure()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        originalArticleTextView.delegate = self
        originalArticleTextView.linkTextAttributes = [NSFontAttributeName: JoyrideFonts.regularFont(ofSize: JoyrideConstants.digestPostFontSize), NSForegroundColorAttributeName: UIColor.red]
        originalArticleTextView.textContainerInset = .zero
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        authorImageView.image = nil
    }

    private func configure() {
        viewModel.authorImageFile?.getDataInBackground { [weak self] (data, error) in
            if let data = data {
                self?.authorImageView.image = UIImage(data: data)
            }
        }

        authorNameLabel.text = viewModel.authorName
        titleLabel.text = viewModel.titleText

        if let text = viewModel.originalArticleText {
            originalArticleTextViewHeight.constant = 18
            originalArticleTextView.attributedText = text
        } else {
            originalArticleTextViewHeight.constant = 0
        }

        bodyLabel.text = viewModel.bodyText

        extraImageView.image = nil
        
        if let extraImageFile = viewModel.extraImageFile {
            
            extraImageViewTopSpace.constant = 0
            extraImageViewBottomSpace.constant = -4
            extraImageViewHeight.constant = 60
            contentView.layoutIfNeeded()
            
            extraImageFile.getDataInBackground(block: { [weak self] (data, error) in
                guard let strongSelf = self else { return }
                if let data = data {
                    strongSelf.extraImageView.image = UIImage(data: data)
                    strongSelf.contentView.layoutIfNeeded()
                }
            })
        } else {
            extraImageViewTopSpace.constant = 4
            extraImageViewBottomSpace.constant = 0
            extraImageViewHeight.constant = 0
            self.contentView.layoutIfNeeded()
        }
    }
}

extension ArticleTableViewCell: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView == originalArticleTextView {
            UIApplication.shared.open(URL, options: [:], completionHandler: nil)
            return true
        }

        return false
    }
}
