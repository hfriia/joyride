//
//  VideoTableCellViewModel.swift
//  Joyride
//
//  Created by Harrison Friia on 7/17/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import Parse

struct VideoTableCellViewModel {

    let authorImageFile: PFFile?
    let authorName: String
    let titleText: String
    let bodyText: String
    let videoURL: URL?
    var videoTitleText: String

    init(digestItem: VideoDigestItem) {
        authorImageFile = digestItem.authorImageFile
        authorName = digestItem.authorName
        titleText = digestItem.titleText
        bodyText = digestItem.bodyText
        videoURL = URL(string: digestItem.videoURL)
        videoTitleText = ""
    }
}
