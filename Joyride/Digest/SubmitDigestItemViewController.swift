//
//  SubmitDigestItemViewController.swift
//  Joyride
//
//  Created by Friia, Harrison on 6/19/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import MessageUI
import Parse

enum Mode {
    case strongerTogether
    case activityAchievements
}

enum MediaType {
    case photo
    case video
}

class SubmitDigestItemViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var contentTextview: UITextView!
    @IBOutlet weak var mediaStackView: UIStackView!

    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!

    @IBOutlet weak var toolbarHeight: NSLayoutConstraint!
    @IBOutlet weak var mediaStackViewHeight: NSLayoutConstraint!

    lazy var imagePicker = UIImagePickerController()
    lazy var imageArray = [UIImage]()
    lazy var videoArray = [URL]()
    
    var displayMode: Mode!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: .UIKeyboardWillShow,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: .UIKeyboardWillHide,
                                               object: nil)

        navBar.titleTextAttributes = [NSFontAttributeName: JoyrideFonts.boldFont(ofSize: 20), NSForegroundColorAttributeName: UIColor.red]

        let backButton = JoyrideControls.backButton(target: self, selector: #selector(backButtonPressed(sender:)))
        navItem.leftBarButtonItem = backButton

        contentTextview.textColor = .gray

        mediaStackView.distribution  = UIStackViewDistribution.equalSpacing
        mediaStackView.alignment = UIStackViewAlignment.top
        mediaStackView.spacing = 5

        imagePicker.delegate = self
        contentTextview.delegate = self

        if displayMode == .activityAchievements {
            navItem.title = "Activity Achievements"
            contentTextview.text = "successful tip or trick?"
            contentTextview.textColor = UIColor.gray
        }
    }

    func backButtonPressed(sender: UIButton) {
        dismiss()
    }
    
    func dismiss() {
        contentTextview.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }

    func canInsertMoreMedia() -> Bool {
        if mediaStackView.arrangedSubviews.count >= 4 {

            let alert = UIAlertController(title: "Alert", message: "You can only attach 4 items.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

            present(alert, animated: true, completion: nil)

            return false
        }

        return true
    }

    func insertImage(image: UIImage) {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaStackView.frame.width, height: 800))
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        imageView.image = image

        // From SO. Helps resize the image view to the height of the image
        // after the image has been resized to fit the width of the screen.
        let widthRatio = imageView.bounds.size.width / (imageView.image?.size.width)!
        let heightRatio = imageView.bounds.size.height / (imageView.image?.size.height)!
        let scale = min(widthRatio, heightRatio)
        let imageHeight = scale * (imageView.image?.size.height)!

        imageView.heightAnchor.constraint(equalToConstant: imageHeight).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: mediaStackView.frame.width).isActive = true

        mediaStackViewHeight.constant = mediaStackView.frame.height + imageHeight
        mediaStackView.addArrangedSubview(imageView)
    }

    func insertVideo(url: URL) {
        let asset = AVURLAsset(url: url)
        let generator = AVAssetImageGenerator(asset: asset)

        generator.appliesPreferredTrackTransform = true

        let time = CMTimeMakeWithSeconds(0, 600)
        var actualtime = CMTime()

        do {
            let cgImage = try generator.copyCGImage(at: time, actualTime: &actualtime)

            let image = UIImage(cgImage: cgImage)

            insertImage(image: image)

        } catch {
            print(error)
        }
    }

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self

        mailComposerVC.setToRecipients(["team@hottopics.co"])
        mailComposerVC.setMessageBody(contentTextview.text, isHTML: false)
        let subject = displayMode == .activityAchievements ? "Activity Achievements" : "Contribute"
        mailComposerVC.setSubject(subject)
        mailComposerVC.delegate = self

        for image in imageArray {
            if let data = UIImageJPEGRepresentation(image, 1) {
                mailComposerVC.addAttachmentData(data, mimeType: "image/jpeg", fileName: "image")
            }
        }

        for url in videoArray {
            do {
                let data = try Data(contentsOf: url)
                mailComposerVC.addAttachmentData(data, mimeType: "video/quicktime", fileName: "video")
            } catch {
                print(error)
            }
        }

        return mailComposerVC
    }
    
    func showActionSheetForMediaType(_ type: MediaType) {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        var cameraActionTitle = String()
        var mediaTypes = [String]()
        
        imagePicker.allowsEditing = false

        if case .photo = type {
            cameraActionTitle = "Take Photo"
            mediaTypes = [kUTTypeLivePhoto as String, kUTTypeImage as String]
            
        } else if case .video = type {
            cameraActionTitle = "Take Video"
            mediaTypes = [kUTTypeMovie as String]
        }
        
        let cameraAction = UIAlertAction(title: cameraActionTitle, style: .default) { action in
            
            self.imagePicker.sourceType = .camera
            self.imagePicker.mediaTypes = mediaTypes
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        let libraryAction = UIAlertAction(title: "Library", style: .default) { action in
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = mediaTypes
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { action in
            sheet.dismiss(animated: true, completion: nil)
        }
        
        sheet.view.tintColor = .red
        
        sheet.addAction(cameraAction)
        sheet.addAction(libraryAction)
        sheet.addAction(cancelAction)
        
        if let field1 = sheet.textFields?.first {
            let newFrame = CGRect(x: field1.frame.origin.x, y: field1.frame.origin.y + 20, width: field1.frame.width, height: field1.frame.height)
            
            field1.frame = newFrame
            sheet.view.layoutIfNeeded()
        }
        
        
        present(sheet, animated: true, completion: nil)
    }

    @IBAction func linkButtonPressed(_ sender: Any) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let user = appDelegate.currentUser else {
            return
        }
        
        let articleItem = ArticleDigestItem(authorImageFile: user.profileImageFile,
                                            authorName: user.name,
                                            titleText: "",
                                            bodyText: contentTextview.text,
                                            position: 999,
                                            approved: false,
                                            originText: nil,
                                            originName: nil,
                                            originLink: nil,
                                            extraImageFile: nil)

        articleItem.saveInBackground { [weak self] (success, error) in
            if success {
                PointsCollector.addPoints(.shareTipsAndTricks)
                DispatchQueue.main.async(execute: {
                    self?.dismiss()
                })
                return
            }
            
            let errorVC = UIAlertController(title: "Error", message: error?.localizedDescription ?? "There was an error submitting your content. Please try again.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                errorVC.dismiss(animated: true, completion: nil)
            }
            
            errorVC.addAction(action)
            
            self?.present(errorVC, animated: true, completion: nil)
        }
        
    }

    @IBAction func addVideo(_ sender: UIButton) {
        guard canInsertMoreMedia() else { return }
        
        showActionSheetForMediaType(.video)
    }

    @IBAction func addPhoto(_ sender: UIButton) {
        guard canInsertMoreMedia() else { return }
        
        showActionSheetForMediaType(.photo)
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
}

// MARK: - UITextViewDelegate
extension SubmitDigestItemViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if contentTextview.text == "share handy tips & tricks!" ||
            contentTextview.text == "successful tip or trick?" {
            contentTextview.text = ""
            contentTextview.textColor = UIColor.darkText
        }
    }
}

// MARK: - UIImagePickerControllerDelegate
extension SubmitDigestItemViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        guard let mediaType = info[UIImagePickerControllerMediaType] as? String else {
            return
        }

        if mediaType == kUTTypeImage as String || mediaType == kUTTypeLivePhoto as String {

            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                imageArray.append(image)
                insertImage(image: image)
                imagePicker.dismiss(animated: true, completion: nil)
            }

        } else if mediaType == kUTTypeMovie as String {

            if let mediaURL = info[UIImagePickerControllerMediaURL] as? URL {
                videoArray.append(mediaURL)
                insertVideo(url: mediaURL)
            } else if let referenceURL = info[UIImagePickerControllerReferenceURL] as? URL {
                videoArray.append(referenceURL)
                insertVideo(url: referenceURL)
            }
            imagePicker.dismiss(animated: true, completion: nil)
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension SubmitDigestItemViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        if result == .sent {
            PointsCollector.addPoints(.shareTipsAndTricks)
        }
        
        controller.dismiss(animated: true) { [weak self] in
            self?.contentTextview.resignFirstResponder()
            self?.dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - Keyboard animations
extension SubmitDigestItemViewController {

    func keyboardWillShow(_ notification: NSNotification) {
        moveView(notification: notification, up: true)
    }

    func keyboardWillHide(_ notification: NSNotification) {
        moveView(notification: notification, up: false)
    }

    func moveView(notification: NSNotification, up: Bool) {

        guard let userInfo = notification.userInfo as? [String:AnyObject] else {
            return
        }

        var height = CGFloat(0)

        if up {
            if let hasHeight = heightFromUserInfo(userInfo) {
                height = hasHeight
            } else {
                return
            }
        }

        animateView(toolbarView!, userInfo: userInfo, toHeight: height)
    }

    func heightFromUserInfo(_ userInfo: [String:AnyObject]) -> CGFloat? {
        guard let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect else {
            return nil
        }

        return keyboardFrame.size.height
    }

    func animateView(_ view: UIView, userInfo: [String:AnyObject], toHeight height: CGFloat) {
        guard let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
            let animationCurveInt = (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue else {
                return
        }

        let animationCurve = UIViewAnimationOptions(rawValue: animationCurveInt)

        view.layoutIfNeeded()
        toolbarHeight.constant = height
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: animationCurve,
                       animations: {
                        self.view.layoutIfNeeded()
        },
                       completion: nil)
    }
}
