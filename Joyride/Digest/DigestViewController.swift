//
//  DigestViewController.swift
//  Joyride
//
//  Created by Harrison Friia on 6/10/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import MGSwipeTableCell
import Parse

class DigestViewController: UIViewController {

    @IBOutlet weak var postButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!

    let articleCellIdentifier = "ArticleTableViewCell"
    let videoCellIdentifier = "VideoTableViewCell"
    let footerCellIdentifier = "FooterCell"
    
    let refreshControl = UIRefreshControl()
    private var loadingView: UIView?
    
    let viewModel = DigestTableViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.white

        automaticallyAdjustsScrollViewInsets = false

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .singleLine
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 199
        
        viewModel.delegate = self

        let settingsButton = JoyrideControls.backButton(target: self, selector: #selector(settingsSwiped(_:)))
        navigationItem.leftBarButtonItem = settingsButton
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(clearBadge),
                                               name: .UIApplicationWillEnterForeground,
                                               object: nil)
        
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        addLoadingView()
    }
    
    func refresh() {
        viewModel.refresh()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = viewModel.date
        clearBadge()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func addLoadingView() {
        let spinnerView = UIView(frame: tableView.frame)
        spinnerView.backgroundColor = .white
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        spinnerView.addSubview(spinner)
        spinner.center = CGPoint(x: spinnerView.frame.width/2, y: spinnerView.frame.height/2)
        
        loadingView = spinnerView
        view.addSubview(spinnerView)
    }
    
    func removeLoadingView() {
        loadingView?.removeFromSuperview()
    }

    func clearBadge() {
        UIApplication.shared.applicationIconBadgeNumber = 0
        let installation = PFInstallation.current()
        installation?.badge = 0
        installation?.saveEventually({ (success, error) in
            if let error = error {
                print(error)
            }
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "strongerTogether" {
            if let vc = segue.destination as? SubmitDigestItemViewController {
                vc.displayMode = .strongerTogether
            }
        }
    }

    @IBAction func settingsSwiped(_ sender: UISwipeGestureRecognizer) {
        let sb = UIStoryboard(name: "Settings", bundle: Bundle.main)
        guard let vc = sb.instantiateInitialViewController() else {
            return
        }

        let settingsTransition = CATransition()
        settingsTransition.duration = 0.3
        settingsTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        settingsTransition.type = kCATransitionPush
        settingsTransition.subtype = kCATransitionFromLeft
        view.window?.layer.add(settingsTransition, forKey: nil)

        navigationController?.show(vc, sender: self)
    }

    @IBAction func unwindToDigest(segue: UIStoryboardSegue) {
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: .UIApplicationWillEnterForeground,
                                                  object: nil)
    }
}

// MARK: - UITableViewDelegate
extension DigestViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 700
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
}

// MARK: - UITableViewDataSource
extension DigestViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let leftSaveButton = MGSwipeButton(title: "Bookmark",
                                            backgroundColor: .red,
                                            callback: nil)
        let rightSaveButton = MGSwipeButton(title: "Bookmark",
                                                 backgroundColor: .red,
                                                 callback: nil)

        if let articleViewModel = viewModel.viewModelForCell(at: indexPath.row) as? ArticleTableCellViewModel {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: articleCellIdentifier, for: indexPath) as? ArticleTableViewCell else {
                return UITableViewCell()
            }

            cell.viewModel = articleViewModel
            // Full width separator line
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero

            cell.delegate = self
            cell.leftButtons = [leftSaveButton]
            cell.rightButtons = [rightSaveButton]

            cell.leftExpansion.expansionColor = .red
            cell.leftExpansion.buttonIndex = 0

            cell.rightExpansion.expansionColor = .red
            cell.rightExpansion.buttonIndex = 0

            return cell
        }

        if let videoViewModel = viewModel.viewModelForCell(at: indexPath.row) as? VideoTableCellViewModel {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: videoCellIdentifier, for: indexPath) as? VideoTableViewCell else {
                return UITableViewCell()
            }

            cell.viewModel = videoViewModel
            // Full width separator line
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero

            cell.delegate = self
            cell.leftButtons = [leftSaveButton]
            cell.rightButtons = [rightSaveButton]

            cell.leftExpansion.expansionColor = .red
            cell.leftExpansion.buttonIndex = 0

            cell.rightExpansion.expansionColor = .red
            cell.rightExpansion.buttonIndex = 0

            return cell
        }

        // Footer cell
        let footerCell = tableView.dequeueReusableCell(withIdentifier: footerCellIdentifier, for: indexPath)

        // Bit of a hack to hide the last cell separator
        footerCell.separatorInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, footerCell.bounds.size.width * 1.2)

        return footerCell
    }
}

extension DigestViewController: MGSwipeTableCellDelegate {
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {

        if let indexPath = tableView.indexPath(for: cell) {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                self?.viewModel.bookmarkItem(at: indexPath.row)
            }
        }

        return true
    }
}

extension DigestViewController: DigestTableViewModelDelegate {
    func digestItemsAvailable(_ items: [DigestItem]) {
        tableView.reloadData()
        refreshControl.endRefreshing()
        removeLoadingView()
    }
    
    func networkErrorReturned(_ error: Error) {
        let alertVC = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { action in
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(action)
        present(alertVC, animated: true, completion: nil)
    }
}
