//
//  ArticleTableCellViewModel.swift
//  Joyride
//
//  Created by Harrison Friia on 7/8/17.
//  Copyright © 2017 Joyride. All rights reserved.
//

import UIKit
import Parse

struct ArticleTableCellViewModel {

    let authorImageFile: PFFile?
    let authorName: String
    let titleText: String
    let bodyText: String
    let extraImageFile: PFFile?
    var originalArticleText: NSAttributedString?

    init(digestItem: ArticleDigestItem) {
        authorImageFile = digestItem.authorImageFile
        authorName = digestItem.authorName
        titleText = digestItem.titleText
        extraImageFile = digestItem.extraImageFile
        bodyText = digestItem.bodyText

        originalArticleText = originalArticleString(from: digestItem)
    }

    private func originalArticleString(from digestItem: ArticleDigestItem) -> NSAttributedString? {

        guard let urlString = digestItem.originLink,
            let originText = digestItem.originText,
            let originName = digestItem.originName else {
                return nil
        }

        let leadingText = NSAttributedString(string: originText, attributes: [NSFontAttributeName: JoyrideFonts.regularFont(ofSize: JoyrideConstants.digestPostFontSize), NSForegroundColorAttributeName: UIColor.darkText])

        let space = NSAttributedString(string: " ")

        let attributedOriginText = NSAttributedString(string: originName, attributes: [NSFontAttributeName: JoyrideFonts.regularFont(ofSize: JoyrideConstants.digestPostFontSize), NSForegroundColorAttributeName: UIColor.red, NSLinkAttributeName: urlString])

        let combination = NSMutableAttributedString()

        combination.append(leadingText)
        combination.append(space)
        combination.append(attributedOriginText)

        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center

        let attributes = [NSParagraphStyleAttributeName: paragraph]

        combination.addAttributes(attributes, range: NSMakeRange(0, combination.length))

        return combination
    }

}
